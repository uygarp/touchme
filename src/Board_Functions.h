/*
 * Board_Functions.h
 *
 *  Created on: Mar 23, 2015
 *      Author: a0221097
 */
#include <stdint.h>
#include <stdbool.h>

#ifndef BOARD_FUNCTIONS_H_
#define BOARD_FUNCTIONS_H_

//void Board_FlashLED(unsigned int numberOfBlinks);

///GPIO_PORT_P4,GPIO_PIN0

//void Disconnect_I2C();
//void Enable_I2C();

void ERM_loop();

void M0_B1();
void M0_B2();
void M0_B3();
void M0_B4();

void M1_B1();
void M1_B2();
void M1_B3();
void M1_B4();

void M2_B1();
void M2_B2();
void M2_B3();
void M2_B4();

void M3_B1();
void M3_B2();
void M3_B3();
void M3_B4();

uint16_t drv_run8(uint8_t actuator, uint8_t WS1, uint8_t WS2, uint8_t WS3, uint8_t WS4, uint8_t WS5, uint8_t WS6, uint8_t WS7, uint8_t WS8);


#define drv_run7(actuator, WS1, WS2, WS3, WS4, WS5, WS6, WS7)    drv_run8(actuator, WS1, WS2, WS3, WS4, WS5, WS6, WS7, 0)
#define drv_run6(actuator, WS1, WS2, WS3, WS4, WS5, WS6)         drv_run7(actuator, WS1, WS2, WS3, WS4, WS5, WS6, 0)
#define drv_run5(actuator, WS1, WS2, WS3, WS4, WS5)              drv_run6(actuator, WS1, WS2, WS3, WS4, WS5, 0)
#define drv_run4(actuator, WS1, WS2, WS3, WS4)                   drv_run5(actuator, WS1, WS2, WS3, WS4, 0)
#define drv_run3(actuator, WS1, WS2, WS3)                        drv_run4(actuator, WS1, WS2, WS3, 0)
#define drv_run2(actuator, WS1, WS2)                             drv_run3(actuator, WS1, WS2, 0)
#define drv_run(actuator, WS1)                                   drv_run2(actuator, WS1, 0)


void TriggerGo();
void ShowModeLED(bool increment);
void Run_Longpress_P();
void Run_Longpress_M();
void Run_Longpress_T();


#endif /* BOARD_FUNCTIONS_H_ */
