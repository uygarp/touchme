
#include "braille.h"
#include <stdlib.h>
#include "Board_Functions.h"


uint8_t braille_table[MAX_CHAR_NUM][6] =
{
   {0,0,1,0,0,0}, // a
   {0,1,1,0,0,0}, // b
   {0,0,1,0,0,1}, // c
   {0,0,1,0,1,1}, // d
   {0,0,1,0,1,0}, // e
   {0,1,1,0,0,1}, // f
   {0,1,1,0,1,1}, // g
   {0,1,1,0,1,0}, // h
   {0,1,0,0,0,1}, // i
   {0,1,0,0,1,1}, // j
   {1,0,1,0,0,0}, // k
   {1,1,1,0,0,0}, // l
   {1,0,1,0,0,1}, // m
   {1,0,1,0,1,1}, // n
   {1,0,1,0,1,0}, // o
   {1,1,1,0,0,1}, // p
   {1,1,1,0,1,1}, // q
   {1,1,1,0,1,0}, // r
   {1,1,0,0,0,1}, // s
   {1,1,0,0,1,1}, // t
   {1,0,1,1,0,0}, // u
   {1,1,1,1,0,0}, // v
   {0,1,0,1,1,1}, // w
   {1,0,1,1,0,1}, // x
   {1,0,1,1,1,1}, // y
   {1,0,1,1,1,0}  // z
};


uint8_t braille2char(uint8_t line1[3], uint8_t line2[3])
{
    uint8_t ii = 0;
    uint8_t ret = NULL;
    for(ii = 0; ii < MAX_CHAR_NUM; ii++)
    {
        if(braille_table[ii][0] == line1[0] &&
            braille_table[ii][1] == line1[1] &&
            braille_table[ii][2] == line1[2] &&
            braille_table[ii][3] == line2[0] &&
            braille_table[ii][4] == line2[1] &&
            braille_table[ii][5] == line2[2])
        {
            ret = 'a' + ii;
            break;
        }
    }
    return ret;
}

uint8_t char2braille(uint8_t chr, uint8_t* line1, uint8_t* line2)
{

    uint8_t ret = 0;

    if(chr >= 'a' && chr <= 'a' + MAX_CHAR_NUM)
    {
        uint8_t ii = chr - 'a';
        *line1 = braille_table[ii][0] * 4 + braille_table[ii][1] * 2 + braille_table[ii][2];
        *line2 = braille_table[ii][3] * 4 + braille_table[ii][4] * 2 + braille_table[ii][5];
        ret = 1;
    }

    return ret;
}
