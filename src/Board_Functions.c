/*
 * Board_Functions.c
 *
 *  Created on: Mar 23, 2015
 *      Author: a0221097
 */

#include "Board_Functions.h"
#include "LED.h"
#include "gpio.h"
#include "board.h"
#include "Haptics.h"
#include "DRV2625.h"
#include "INA231_EEPR.h"
#include "HapticErrNo.h"
#include "Actuator_Waveforms.h"

uint16_t hapticErr;
uint8_t hrtcount=0;
int xs= 0, c=0;
extern volatile uint8_t modecounter;

void TriggerGo()
{
    int ret = 0;
    uint8_t RegValue = 0;

    ret = StdI2C_P_RX_Single(DRV_ADDR, GOBIT, &RegValue, &hapticErr);
    if((ret == 1)&&(hapticErr == NO_ERROR))
    {
        if(RegValue == 0)
            StdI2C_P_TX_Single(DRV_ADDR, GOBIT , GO , &hapticErr);
        else
            StdI2C_P_TX_Single(DRV_ADDR, GOBIT , STOP , &hapticErr);
    }
}

void ShowModeLED(bool increment)
{
    if(increment){
        modecounter ++;
        if(modecounter > 4) modecounter = 0;
    }else{
        if(modecounter > 0) modecounter --;
        else modecounter = 4;
    }

	LED_Off(LED_PORT_M, LED_MODEALL);

	switch(modecounter)
	{
	case 1:
		//modecounter++;
		LED_On(LED_PORT_M, LED_M0);
	//	GPIO_toggleOutputOnPin(DRV_NRST);
//		timerdelay(1000);
		break;

	case 2:
		//modecounter++;
		LED_On(LED_PORT_M, LED_M1);
		break;
	case 3:
		//modecounter=0;
		LED_On(LED_PORT_M, LED_M2);
		break;
	case 4:
		LED_On(LED_PORT_M, LED_M3);
		break;
	}
}

void Run_Longpress_P()
{
	LED_Off(EEPR_WPT);

	Board_FlashLED(2);
}

void Run_Longpress_M()
{
	Board_FlashLED(3);
	LED_On(LED_PORT_D, LED_DALL);
	LED_Off(LED_PORT_D, LED_D2);
	LED_Off(LED_PORT_M, LED_MODEALL);
	LED_On(LED_PORT_M, (LED_M0 + LED_M1));


///	Disconnect_I2C();
	///__bis_SR_register(LPM0_bits + GIE);
}

void Run_Longpress_T()
{
	LED_On(LED_PORT_D, LED_DT);
	Board_FlashLED(3);
	//Enable_I2C();

	//GPIO_setAsInputPin(GPIO_PORT_P4, GPIO_PIN1 + GPIO_PIN2 ); high z the I2C lines

}
void Disconnect_I2C()
{
	Board_FlashLED(2);
	GPIO_setAsInputPin(GPIO_PORT_P4,GPIO_PIN1 + GPIO_PIN2);


}
void Enable_I2C()
{
	initI2C();
}

void wait2(uint8_t wait_ms);
void wait2(uint8_t wait_ms)
{
    int k = 0;
    for(k = 0; k < wait_ms; k++)
    {
        timerdelay(33.3);
    }
}

void ERM_click_openloop()
{
    DRV_Enable();
    DRV_Send_Actuator_Settings(Actuator_ERM);
    Haptics_LoadSwitchSelect(Actuator_ERM);
    StdI2C_P_TX_Single(DRV_ADDR, DRV_SET2,ERM_Mode|Open_Loop , &hapticErr);
    //StdI2C_P_TX_Single(DRV_ADDR, LRA_OL_PeriodL, 0xEF, &hapticErr);
    StdI2C_P_TX_Single(DRV_ADDR, DRV_SET1 , EXT_Pulse_TRG, &hapticErr);
    //StdI2C_P_TX_Single(DRV_ADDR, RTP_Input , 0x7F, &hapticErr);
    //StdI2C_P_TX_Single(DRV_ADDR, LRA_Option1 ,LRA_Waveshape_Sine, &hapticErr);
    StdI2C_P_TX_Single(DRV_ADDR, GOBIT , GO, &hapticErr);
    for(c = 0; c < 50; c++)
    {
        timerdelay(33.3);
    }
    //wait2(10000);
    StdI2C_P_TX_Single(DRV_ADDR, GOBIT, STOP, &hapticErr);
}

void ERM_RTP_buzz_closeloop()
{
    DRV_Enable();
    DRV_Send_Actuator_Settings(Actuator_ERM);
    Haptics_LoadSwitchSelect(Actuator_ERM);
    StdI2C_P_TX_Single(DRV_ADDR, DRV_SET2,ERM_Mode|Closed_Loop , &hapticErr);
    //StdI2C_P_TX_Single(DRV_ADDR, LRA_OL_PeriodL, 0xEF, &hapticErr);
    StdI2C_P_TX_Single(DRV_ADDR, DRV_SET1 , RTP_Mode, &hapticErr);
    //StdI2C_P_TX_Single(DRV_ADDR, RTP_Input , 0x7F, &hapticErr);
    //StdI2C_P_TX_Single(DRV_ADDR, LRA_Option1 ,LRA_Waveshape_Sine, &hapticErr);
    StdI2C_P_TX_Single(DRV_ADDR, GOBIT , GO, &hapticErr);
    for(c = 0; c < 50; c++)
    {
        timerdelay(33.3);
    }
    //wait2(10000);
    StdI2C_P_TX_Single(DRV_ADDR, GOBIT, STOP, &hapticErr);
}


void ERM_loop()
{
    int ii = 1;
    for(ii = 1; ii <= SmoothHum5Nokickorbrakepulse_10; ii++)
    {
        if(true)
        {
            ERM_click_openloop();
            //ERM_RTP_buzz_closeloop();
        }
        else
        {
            DRV_Enable();
            DRV_Send_Actuator_Settings(Actuator_LRA);
            Haptics_LoadSwitchSelect(Actuator_LRA);

            StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ1, ii  , &hapticErr);
            StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ2, 0x00, &hapticErr);
            StdI2C_P_TX_Single(DRV_ADDR, GOBIT, GO, &hapticErr);
        }
        //wait2(50000);
        timerdelay(200000);
    }
}

/* Mode 0  Functions */
void M0_B1()
{
	LED_Toggle(LED_PORT_D, LED_D1);
	DRV_Enable();
	DRV_Send_Actuator_Settings(Actuator_ERM);
	Haptics_LoadSwitchSelect(Actuator_ERM);

	StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ1, SharpClick_100  , &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ2, TransitionRampDownShortSmooth1_100to0, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ3, 0x00, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, GOBIT, GO, &hapticErr);
	//DRV_Poll();
	//timerdelay(1000);
	//LED_Toggle(LED_PORT_D, LED_D1);
}

void M0_B2()
{
	LED_Toggle(LED_PORT_D, LED_D2);
	DRV_Enable();
	DRV_Send_Actuator_Settings(Actuator_LRA);
	Haptics_LoadSwitchSelect(Actuator_LRA);

	StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ1, SharpClick_100  , &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ2, TransitionRampDownShortSmooth1_100to0, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, GOBIT, GO, &hapticErr);
	//DRV_Poll();
	//timerdelay(1000);
	//LED_Toggle(LED_PORT_D, LED_D2);
}

void M0_B3()
{
	LED_Toggle(LED_PORT_D, LED_D3);
	DRV_Enable();
	DRV_Send_Actuator_Settings(Actuator_ERM);
	Haptics_LoadSwitchSelect(Actuator_ERM);

	StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ1, PulsingSharp1_100  , &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ2, 0x00, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, GOBIT, GO, &hapticErr);
	//DRV_Poll();
	//timerdelay(1000);
	//LED_Toggle(LED_PORT_D, LED_D3);

}

void M0_B4()
{
	LED_Toggle(LED_PORT_D, LED_D4);
	DRV_Enable();
	DRV_Send_Actuator_Settings(Actuator_LRA);
	Haptics_LoadSwitchSelect(Actuator_LRA);

	StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ1, PulsingSharp1_100  , &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ2, 0x00, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, GOBIT, GO, &hapticErr);
	//DRV_Poll();
	//timerdelay(1000);
	//LED_Toggle(LED_PORT_D, LED_D4);

}

void M1_B1()
{
	LED_Toggle(LED_PORT_D, LED_D1);
	DRV_Enable();
	DRV_Send_Actuator_Settings(Actuator_ERM);
	Haptics_LoadSwitchSelect(Actuator_ERM);

	StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ1, SoftBump_100  , &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ2, 0x00, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, GOBIT, GO, &hapticErr);
	//DRV_Poll();
	//timerdelay(1000);
	//LED_Toggle(LED_PORT_D, LED_D1);
}

void M1_B2()
{
	LED_Toggle(LED_PORT_D, LED_D2);
	DRV_Enable();
	DRV_Send_Actuator_Settings(Actuator_LRA);
	Haptics_LoadSwitchSelect(Actuator_LRA);

	StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ1, SoftBump_100  , &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ2, 0x00, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, GOBIT, GO, &hapticErr);
	//DRV_Poll();
	//timerdelay(1000);
	//LED_Toggle(LED_PORT_D, LED_D2);
}

void M1_B3()
{
	//LED_Toggle(LED_PORT_D, LED_D3);
	//DRV_Enable();
	DRV_Send_Actuator_Settings(Actuator_ERM);
	Haptics_LoadSwitchSelect(Actuator_ERM);

	StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ1, DoubleClick_100  , &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ2, 0x00, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, GOBIT, GO, &hapticErr);
	DRV_Poll();
	//timerdelay(1000);
	//LED_Toggle(LED_PORT_D, LED_D3);
}

void M1_B4()
{
	//LED_Toggle(LED_PORT_D, LED_D4);
	//DRV_Enable();
	DRV_Send_Actuator_Settings(Actuator_LRA);
	Haptics_LoadSwitchSelect(Actuator_LRA);

	StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ1, DoubleClick_100  , &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ2, 0x00, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, GOBIT, GO, &hapticErr);
	DRV_Poll();
	//timerdelay(1000);
	//LED_Toggle(LED_PORT_D, LED_D4);
}

void M2_B1()
{
	LED_Toggle(LED_PORT_D, LED_D1);
	DRV_Enable();
	DRV_Send_Actuator_Settings(Actuator_ERM);
	Haptics_LoadSwitchSelect(Actuator_ERM);

	for(hrtcount= 0; hrtcount<3; hrtcount++)
	{
		StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ1, DoubleClick_100  , &hapticErr);
		StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ2, 0x00, &hapticErr);
		StdI2C_P_TX_Single(DRV_ADDR, GOBIT, GO, &hapticErr);
		DRV_Poll();

		timerdelay(20000);
	}

	LED_Toggle(LED_PORT_D, LED_D1);
}

void M2_B2()
{
	LED_Toggle(LED_PORT_D, LED_D2);
	DRV_Enable();
	DRV_Send_Actuator_Settings(Actuator_LRA);
	Haptics_LoadSwitchSelect(Actuator_LRA);

	for(hrtcount= 0; hrtcount<3; hrtcount++)
	{
		StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ1, DoubleClick_100  , &hapticErr);
		StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ2, 0x00, &hapticErr);
		StdI2C_P_TX_Single(DRV_ADDR, GOBIT, GO, &hapticErr);
		DRV_Poll();

		timerdelay(20000);
	}

	LED_Toggle(LED_PORT_D, LED_D2);
}

void M2_B3()
{
	LED_Toggle(LED_PORT_D, LED_D3);
	DRV_Enable();
	DRV_Send_Actuator_Settings(Actuator_LRA);
	Haptics_LoadSwitchSelect(Actuator_LRA);

	StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ1, BuzzAlert750ms  , &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ2, 0x00, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, GOBIT, GO, &hapticErr);
	DRV_Poll();
	timerdelay(1000);
	LED_Toggle(LED_PORT_D, LED_D3);
}

void M2_B4()
{
	LED_Toggle(LED_PORT_D, LED_D4);
	DRV_Enable();
	DRV_Send_Actuator_Settings(Actuator_ERM);
	Haptics_LoadSwitchSelect(Actuator_ERM);
	StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ1, BuzzAlert750ms  , &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ2, 0x00, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, GOBIT, GO, &hapticErr);
	DRV_Poll();
	timerdelay(1000);
	LED_Toggle(LED_PORT_D, LED_D4);
}

void M3_B1()
{
	LED_Toggle(LED_PORT_D, LED_D1);
	DRV_Enable();
	DRV_Send_Actuator_Settings(Actuator_LRA);
	Haptics_LoadSwitchSelect(Actuator_LRA);
	StdI2C_P_TX_Single(DRV_ADDR, DRV_SET2,LRA_Mode|Closed_Loop , &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, DRV_SET1 , INTZ_TRG| RTP_Mode, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, RTP_Input , 0x7F, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, LRA_Option1 ,LRA_Waveshape_Sine, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, GOBIT , GO , &hapticErr);
	for(c = 0; c<10; c++)
	{
		timerdelay(2000);
	}
	StdI2C_P_TX_Single(DRV_ADDR, GOBIT , STOP , &hapticErr);
	LED_Toggle(LED_PORT_D, LED_D1);
}

void M3_B2()
{
	LED_Toggle(LED_PORT_D, LED_D2);
	DRV_Enable();
	DRV_Send_Actuator_Settings(Actuator_LRA);
	Haptics_LoadSwitchSelect(Actuator_LRA);
	StdI2C_P_TX_Single(DRV_ADDR, DRV_SET2,LRA_Mode|Open_Loop|Auto_BRK_OLoop|Auto_BRK_STDBY_CHK , &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, LRA_OL_PeriodL, 0xEF, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, DRV_SET1 , INTZ_TRG| RTP_Mode, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, RTP_Input , 0x7F, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, LRA_Option1 ,LRA_Waveshape_Square, &hapticErr);
	uint8_t c = 0;
	for(c = 0; c<2; c++)
	{
		StdI2C_P_TX_Single(DRV_ADDR, GOBIT ,0x01 , &hapticErr);
		timerdelay(2000);
		StdI2C_P_TX_Single(DRV_ADDR, GOBIT ,0x00 , &hapticErr);
		timerdelay(2000);
	}
	LED_Toggle(LED_PORT_D, LED_D2);

}

void M3_B3()
{
	LED_Toggle(LED_PORT_D, LED_D3);
	DRV_Enable();
	DRV_Send_Actuator_Settings(Actuator_LRA);
	Haptics_LoadSwitchSelect(Actuator_LRA);
	StdI2C_P_TX_Single(DRV_ADDR, DRV_SET2,LRA_Mode|Open_Loop , &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, LRA_OL_PeriodL, 0xEF, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, DRV_SET1 , INTZ_TRG| RTP_Mode, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, RTP_Input , 0x7F, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, LRA_Option1 ,LRA_Waveshape_Sine, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, GOBIT , GO, &hapticErr);
	for(c = 0; c<10; c++)
	{
		timerdelay(2000);
	}
	StdI2C_P_TX_Single(DRV_ADDR, GOBIT, STOP, &hapticErr);
	LED_Toggle(LED_PORT_D, LED_D3);

}

void M3_B4()
{
	LED_Toggle(LED_PORT_D, LED_D4);
	DRV_Enable();
	DRV_Send_Actuator_Settings(Actuator_LRA);
	Haptics_LoadSwitchSelect(Actuator_LRA);
	StdI2C_P_TX_Single(DRV_ADDR, DRV_SET2,LRA_Mode|Open_Loop|Auto_BRK_OLoop_DIS , &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, LRA_OL_PeriodL, 0xEF, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, DRV_SET1 , INTZ_TRG| RTP_Mode, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, RTP_Input , 0x7F, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, LRA_Option1 ,LRA_Waveshape_Square, &hapticErr);
	//uint8_t c = 0;
	for(c = 0; c<2; c++)
	{
		StdI2C_P_TX_Single(DRV_ADDR, GOBIT ,0x01 , &hapticErr);
		timerdelay(2000);
		StdI2C_P_TX_Single(DRV_ADDR, GOBIT ,0x00 , &hapticErr);
		timerdelay(2000);
	}


	LED_Toggle(LED_PORT_D, LED_D4);
}

uint16_t drv_run8(uint8_t actuator, uint8_t WS1, uint8_t WS2, uint8_t WS3, uint8_t WS4, uint8_t WS5, uint8_t WS6, uint8_t WS7, uint8_t WS8)
{
    DRV_Enable();
    DRV_Send_Actuator_Settings(actuator);
    Haptics_LoadSwitchSelect(actuator);

    if(WS1)
    {
        StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ1, WS1  , &hapticErr);
        if(WS2)
        {
            StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ2, WS2, &hapticErr);
            if(WS3)
            {
                StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ3, WS3, &hapticErr);
                if(WS4)
                {
                    StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ4, WS4, &hapticErr);
                    if(WS5)
                    {
                        StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ5, WS5, &hapticErr);
                        if(WS6)
                        {
                            StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ6, WS6, &hapticErr);
                            if(WS7)
                            {
                                StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ7, WS7, &hapticErr);
                                if(WS8)
                                {
                                    StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ2, WS8, &hapticErr);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    StdI2C_P_TX_Single(DRV_ADDR, GOBIT, GO, &hapticErr);
    //DRV_Poll();
    return hapticErr;
}


