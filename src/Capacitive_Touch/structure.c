#include "../Capacitive_Touch/structure.h"


#define CHARACTERIZE
//#define APPLICATION

const struct Element buttonm = {

		.inputBits = CBIMSEL_0,
		// measured for a 1Mhz SMCLK
		.maxResponse = 840,
		.threshold = 400,
		.referenceNumber = BUTTONM,
		.sequenceNumber = 1
};
const struct Element buttonp = {

		.inputBits = CBIMSEL_5,
		// measured for a 1Mhz SMCLK
		.maxResponse = 840,
		.threshold = 400,
		.referenceNumber = BUTTONP,
		.sequenceNumber = 1
};

const struct Element buttont = {

		.inputBits = CBIMSEL_4,
		// measured for a 1Mhz SMCLK
		.maxResponse = 840,
		.threshold = 300,
		.referenceNumber = BUTTONT,
		.sequenceNumber = 1
};

#define CHARACTERIZE
	//#define APPLICATION

	const struct Element inner0 = {

		// .inputPxselRegister = (uint8_t *)&P3SEL,
		//    .inputPxsel2Register = (uint8_t *)&P3SEL2,
		.inputBits = CBIMSEL_3,
		.maxResponse = 600,


	#ifdef CHARACTERIZE
		.threshold = 1
	#endif
	#ifdef APPLICATION
		.threshold = 152
	#endif
	};
	const struct Element inner1 = {

		//  .inputPxselRegister = (uint8_t *)&P2SEL,
		// .inputPxsel2Register = (uint8_t *)&P2SEL2,
		.inputBits = CBIMSEL_6,
		.maxResponse = 600,
	#ifdef CHARACTERIZE
		.threshold = 1
	#endif
	#ifdef APPLICATION
		.threshold = 200
	#endif
	};
	const struct Element inner2 = {

		//   .inputPxselRegister = (uint8_t *)&P3SEL,
		//  .inputPxsel2Register = (uint8_t *)&P3SEL2,
		.inputBits = CBIMSEL_2,
		.maxResponse = 600,
	#ifdef CHARACTERIZE
		.threshold = 1
	#endif
	#ifdef APPLICATION
		.threshold = 180
	#endif
	};
	const struct Element inner3 = {

		//     .inputPxselRegister = (uint8_t *)&P3SEL,
		//    .inputPxsel2Register = (uint8_t *)&P3SEL2,
		.inputBits = CBIMSEL_1,
		.maxResponse = 600,
	#ifdef CHARACTERIZE
		.threshold = 1
	#endif
	#ifdef APPLICATION
		.threshold = 170
	#endif
	};

const struct Element outer0 = {
		//              .inputPxselRegister = (uint8_t *)&P3SEL,
		//              .inputPxsel2Register = (uint8_t *)&P3SEL2,
		//              .inputBits = BIT5,
		////              .maxResponse = 452,
		//              .maxResponse = 655+20,
#ifdef CHARACTERIZE
		.threshold = 0
#endif
#ifdef APPLICATION
		.threshold = 20
#endif
};

const struct Element outer1 = {
		//              .inputPxselRegister = (uint8_t *)&P3SEL,
		//              .inputPxsel2Register = (uint8_t *)&P3SEL2,
		//              .inputBits = BIT6,
		////              .maxResponse = 453,
		//              .maxResponse = 655+20,
#ifdef CHARACTERIZE
		.threshold = 0
#endif
#ifdef APPLICATION
		.threshold = 20
#endif
};
const struct Element outer2 = {
		//
		//              .inputPxselRegister = (uint8_t *)&P2SEL,
		//              .inputPxsel2Register = (uint8_t *)&P2SEL2,
		//              .inputBits = BIT5,
		////              .maxResponse = 490,
		//              .maxResponse = 655+20,
#ifdef CHARACTERIZE
		.threshold = 0
#endif
#ifdef APPLICATION
		.threshold = 20
#endif
};
const struct Element outer3 = {

		//              .inputPxselRegister = (uint8_t *)&P2SEL,
		//              .inputPxsel2Register = (uint8_t *)&P2SEL2,
		//              .inputBits = BIT4,
		//              .maxResponse = 655+20,
#ifdef CHARACTERIZE
		.threshold = 0
#endif
#ifdef APPLICATION
		.threshold = 20
#endif
};
const struct Element outer4 = {

		//              .inputPxselRegister = (uint8_t *)&P2SEL,
		//              .inputPxsel2Register = (uint8_t *)&P2SEL2,
		//              .inputBits = BIT1,
		//              .maxResponse = 655+20,
#ifdef CHARACTERIZE
		.threshold = 0
#endif
#ifdef APPLICATION
		.threshold = 20
#endif
};
const struct Element outer5 = {

		//              .inputPxselRegister = (uint8_t *)&P2SEL,
		//              .inputPxsel2Register = (uint8_t *)&P2SEL2,
		//              .inputBits = BIT0,
		//              .maxResponse = 655+20,
#ifdef CHARACTERIZE
		.threshold = 0
#endif
#ifdef APPLICATION
		.threshold = 20
#endif
};
const struct Element outer6 = {

		//              .inputPxselRegister = (uint8_t *)&P3SEL,
		//              .inputPxsel2Register = (uint8_t *)&P3SEL2,
		//              .inputBits = BIT0,
		//              .maxResponse = 655+20,
#ifdef CHARACTERIZE
		.threshold = 0
#endif
#ifdef APPLICATION
		.threshold = 20
#endif
};
const struct Element outer7 = {

		//              .inputPxselRegister = (uint8_t *)&P2SEL,
		//              .inputPxsel2Register = (uint8_t *)&P2SEL2,
		//              .inputBits = BIT2,
		//              .maxResponse = 655+20,
#ifdef CHARACTERIZE
		.threshold = 50
#endif
#ifdef APPLICATION
		.threshold = 20
#endif
};
const struct Sensor innerWheel =
{
	.halDefinition = RO_COMPB_TA1_WDTA,
	.numElements = 4,
	.baseOffset = 0,
	.points = 12,
	.sensorThreshold =5,
	.arrayPtr[0] = &inner0,
	.arrayPtr[1] = &inner1,
	.arrayPtr[2] = &inner2,
	.arrayPtr[3] = &inner3,
	.cboutTAxDirRegister = (uint8_t *)&P1DIR, // PxDIR
	.cboutTAxSelRegister = (uint8_t *)&P1SEL, // PxSEL
	.cboutTAxBits = BIT6, // P1.6
	.measGateSource = GATE_WDTA_SMCLK,
	.accumulationCycles = WDTA_GATE_8192
};

const struct Sensor buttons =
{
		.halDefinition = RO_COMPB_TA1_WDTA,
		.numElements = 3,
		.baseOffset = 4,
		.cbpdBits = (BIT0 + BIT5 + BIT4), 	// CB0,CB1
		// Pointer to elements
		.arrayPtr[0] = &buttonm,  // point to first element
		.arrayPtr[1] = &buttonp,  //
		.arrayPtr[2] = &buttont,
		.cboutTAxDirRegister = (uint8_t *)&P1DIR, // PxDIR
		.cboutTAxSelRegister = (uint8_t *)&P1SEL, // PxSEL
		.cboutTAxBits = BIT6, // P1.6
		// Timer Information
		.measGateSource= GATE_WDTA_SMCLK,
		.accumulationCycles= WDTA_GATE_8192

};
//
//const struct Sensor outerWheel =
//{
//		.halDefinition = RO_COMPB_TA1_WDTA,
//		.numElements = 8,
//		.baseOffset = 4,
//		.points = 100,
//		.sensorThreshold = 35,
//		.arrayPtr[0] = &outer0,
//		.arrayPtr[1] = &outer1,
//		.arrayPtr[2] = &outer2,
//		.arrayPtr[3] = &outer3,
//		//			.arrayPtr[4] = &outer4,
//		//			.arrayPtr[5] = &outer5,
//		//			.arrayPtr[6] = &outer6,
//		//			.arrayPtr[7] = &outer7,
//		.measGateSource = TIMER_SMCLK,
//		.accumulationCycles = 2200
//};
