#ifndef BRAILLE_H_
#define BRAILLE_H_

#include "Board_Functions.h"

#define MAX_CHAR_NUM 26

uint8_t braille2char(uint8_t line1[3], uint8_t line2[3]);
uint8_t char2braille(uint8_t chr, uint8_t* line1, uint8_t* line2);

#endif
