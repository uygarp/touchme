/*
 * DRV2625.h
 *
 *  Created on: Mar 11, 2015
 *      Author: a0221097
 */
/****************************************************************************
 * DRV262x Header File for Register Defines
 * Updated 12/21/2012
 * // Functional Name: DRV
 *
 ****************************************************************************/


#define DRV_ADDR		(0x5A)

/****************************************************************************/
/* Information Register */
/****************************************************************************/
#define	DRV262x_INFO	(0x00)
/*** Register Bits ***/
#define Chip_ID			(0xF0)
#define Rev				(0x0F)
/****************************************************************************/


/****************************************************************************/
/* Status Register */
/****************************************************************************/
#define	DRV_Status	(0x01)
/*** Register Bits ***/
#define Diag_Result		(0x80)
#define PRG_ERROR		(0x10)
#define Process_Done	(0x08)
#define UVLO			(0x04)
#define Over_Temp		(0x02)
#define OC_Detect		(0x01)
/****************************************************************************/


/****************************************************************************/
/* Interrupt Register */
/****************************************************************************/
#define DRV_Interrupt	(0x02)
/*** Register Bits ***/
#define OC_Detect		(0x01)
#define Over_TEMP		(0x02)
#define UVLO			(0x04)
#define PRC_Done		(0x08)
#define PRG_Error		(0x10)
/****************************************************************************/


/* Diagnostic Register */
/****************************************************************************/
#define Diag_Z_Result (0x03)
/****************************************************************************/

/****************************************************************************/
/* VBAT Register */
/****************************************************************************/
#define DRV_VBAT			(0x04)
/****************************************************************************/

/****************************************************************************/
/* LRA Period Register */
/****************************************************************************/
#define LRA_PeriodM			(0x05)
#define LRA_PeriodL			(0x06)
/****************************************************************************/

/****************************************************************************/
/* Setting1 Register */
/****************************************************************************/
#define DRV_SET1			(0x07)
/*** Register Bits ***/
#define I2C_BCAST_Enable	(0x80)
#define I2C_BCAST_Disable	(0x00)
#define LRA_PERIOD_AVG_DIS	(0x40)
#define LRA_PERIOD_AVG_EN	(0x00)
#define SLEW_RATE_FAST		(0x10)
#define SLEW_RATE_SLOW		(0x00)

// TRIG_PIN_FN
#define INTZ_TRG			(0x08)
#define EXT_LVL_TRG			(0x04)
#define EXT_Pulse_TRG		(0x00)
// MODE
#define ACAL_Routine		(0x03)
#define DIAG_Routine		(0x02)
#define WVFM_Sequencer		(0x01)
#define RTP_Mode			(0x00)
/****************************************************************************/

/****************************************************************************/
/* Setting2 Register */
/****************************************************************************/
#define DRV_SET2			(0x08)
/*** Register Bits ***/
#define ERM_Mode			(0x00)
#define LRA_Mode			(0x80)
#define Open_Loop			(0x40)
#define Closed_Loop			(0x00)
#define Pseudo_Closed_Loop	(0x20)
#define Full_Closed_Loop	(0x00)
#define Auto_BRK_OLoop		(0x10)
#define Auto_BRK_OLoop_DIS	(0x00)
#define Auto_BRK_STDBY_CHK	(0x08)
#define Auto_BRK_STDBY_IMM	(0x00)
#define Input_Slope_CHK_EN	(0x04)
#define HIZ_				(0x01)  //TI-BIT
/****************************************************************************/

/****************************************************************************/
/* Setting3 Register */
/****************************************************************************/
#define DRV_SET3				(0x09)
/*** Register Bits ***/
#define BAT_Life_EXT_LVL12_EN	(0x80)
#define BAT_Life_EXT_LVL1_EN	(0x40)
#define BAT_Life_EXT_Disable	(0x00)
#define UVLO_Threshold_3p2		(0x07)
#define UVLO_Threshold_3p1		(0x06)
#define UVLO_Threshold_3p0		(0x05)
#define UVLO_Threshold_2p9		(0x04)
#define UVLO_Threshold_2p8		(0x03)
#define UVLO_Threshold_2p7		(0x02)
#define UVLO_Threshold_2p6		(0x01)
#define UVLO_Threshold_2p5		(0x00)
/****************************************************************************/

/****************************************************************************/
/* Battery Life External Level One Register */
/****************************************************************************/
#define BAT_Life_EXT_LVL1		(0x0A)
/****************************************************************************/

/****************************************************************************/
/* Go Bit */
/****************************************************************************/
#define GOBIT							(0x0C)
#define GO						(0x01)
#define STOP					(0x00)
/****************************************************************************/
/****************************************************************************/
/* Selection Register */
/****************************************************************************/
#define Selection		(0x0D)
/*** Register Bits ***/
#define ERM_Library_OpenLoop	(0xC0)
#define LRA_Library_ClosedLoop	(0x80)
#define RAM1					(0x40)
#define RAM2					(0x00)
#define Playback_Interval_1ms	(0x20)
#define Playback_Interval_5ms	(0x00)
#define Strength_25p			(0x03)
#define Strength_50p			(0x02)
#define Strength_75p			(0x01)
#define Strength_100p			(0x00)
/****************************************************************************/

/****************************************************************************/
/* RTP Input */
/****************************************************************************/
#define RTP_Input				(0x0E)
/****************************************************************************/

/****************************************************************************/
/* Setting1 Register */
/****************************************************************************/
#define DRV_SET1			(0x07)
/*** Register Bits ***/
#define I2C_BCAST_Enable	(0x80)
#define I2C_BCAST_Disable	(0x00)

#define LRA_PERIOD_AVG_DIS	(0x40)
#define LRA_PERIOD_AVG_EN	(0x00)

#define SLEW_RATE_FAST		(0x10)
#define SLEW_RATE_SLOW		(0x00)

// TRIG_PIN_FN
#define INTZ_TRG			(0x08)
#define EXT_LVL_TRG			(0x04)
#define EXT_Pulse_TRG		(0x00)
// MODE
#define ACAL_Routine		(0x03)
#define DIAG_Routine		(0x02)
#define WVFM_Sequencer		(0x01)
#define RTP_Mode			(0x00)
/****************************************************************************/

/****************************************************************************/
/* Setting2 Register */
/****************************************************************************/
#define DRV_SET2			(0x08)
/*** Register Bits ***/
#define ERM_Mode			(0x00)
#define LRA_Mode			(0x80)
#define Open_Loop			(0x40)
#define Closed_Loop			(0x00)
#define Pseudo_Closed_Loop	(0x20)
#define Full_Closed_Loop	(0x00)
#define Auto_BRK_OLoop		(0x10)
#define Auto_BRK_OLoop_DIS	(0x00)
#define Auto_BRK_STDBY_CHK	(0x08)
#define Input_Slope_CHK_DIS	(0x00)
#define Input_Slope_CHK_EN	(0x04)
#define HIZ_				(0x01)  //TI-BIT
/****************************************************************************/

/****************************************************************************/
/* Setting3 Register */
/****************************************************************************/
#define Setting3				(0x09)
/*** Register Bits ***/
#define BAT_Life_EXT_LVL12_EN	(0x80)
#define BAT_Life_EXT_LVL1_EN	(0x40)
#define BAT_Life_EXT_Disable	(0x00)

#define UVLO_Threshold_3p2		(0x07)
#define UVLO_Threshold_3p1		(0x06)
#define UVLO_Threshold_3p0		(0x05)
#define UVLO_Threshold_2p9		(0x04)
#define UVLO_Threshold_2p8		(0x03)
#define UVLO_Threshold_2p7		(0x02)
#define UVLO_Threshold_2p6		(0x01)
#define UVLO_Threshold_2p5		(0x00)
/****************************************************************************/

/* Battery Life External Level One Register */
#define BAT_Life_EXT_LVL1		(0x0A)

/****************************************************************************/
/* Selection Register */
/****************************************************************************/
#define DRV_SEL		(0x0D)
/*** Register Bits ***/
#define ERM_Library_OpenLoop	(0xC0)
#define LRA_Library_ClosedLoop	(0x80)
#define RAM1					(0x40)
#define RAM2					(0x00)
#define Playback_Interval_1ms	(0x20)
#define Playback_Interval_5ms	(0x00)
#define Strength_25p			(0x03)
#define Strength_50p			(0x02)
#define Strength_75p			(0x01)
#define Strength_100p			(0x00)
/****************************************************************************/

/* Over Drive Time Offset */
#define ODT_Offset							(0x1A)
/* Sustain Positive  */
#define SPT									(0x1B)
/* Sustain Negative */
#define SNT									(0x1C)
/* Braking Offset */
#define BRT									(0x1D)
/* Rated Voltage */
#define Rated_Voltage						(0x1F)
/* Overdrive Clamp Voltage */
#define OD_Clamp							(0x20)
/* AutoCalibration Voltage Compensation */
#define ACAL_COMP							(0x21)
/* AutoCalibration Back EMF */
#define ACAL_BEMF							(0x22)
//
///****************************************************************************/

// AutoCal Duration
///****************************************************************************/
#define AutoCal_Time 					(0x2A)
#define Time_250ms						(0x00)
#define Time_500ms						(0x01)
#define Time_1000ms						(0x02)
#define Time_Trigger					(0x03)

///* Control Register */

#define LRA_Control 				(0x2C)
//Data::
#define Auto_Open_Loop_Enable		(0x80)
#define Auto_Open_Loop_Disable		(0x00)
#define Auto_Open_Loop_Counter_6	(0x30)
#define Auto_Open_Loop_Counter_5	(0x20)
#define Auto_Open_Loop_Counter_4	(0x20)
#define Auto_Open_Loop_Counter_3	(0x00)
#define LRA_Sine_Wave				(0x01)
#define LRA_Square_Wave				(0x00)


///////_---------------------------------------///////
// Waveform Structure Type Definition
//typedef struct Haptics_Waveform {
//	const unsigned char 	inputMode; 		// See input mode
//	const unsigned char		length;			// Size of array in bytes
//	const unsigned char* 	data;			// Pointer to waveform array data (waveform array is in (amplitude, time) pairs
//} Waveform;

// ERM Overdrive(Open Loop), LRA Overdrive (Closed Loop)
#define Voltage_1p3	(0x3B)
#define Voltage_1p5	(0x44)
#define Voltage_1p8	(0x52)
#define Voltage_2p0	(0x5B)
#define Voltage_2p3	(0x69)
#define Voltage_2p5	(0x72)
#define Voltage_2p7	(0x7B)
#define Voltage_3p0	(0x89)
#define Voltage_3p3	(0x96)
#define Voltage_3p6 (0xA4)
#define Voltage_5p0	(0xE4)

// ERM Overdrive - Closed Loop
#define Voltage_ERM_OD_CL_1p3	(0x43)
#define Voltage_ERM_OD_CL_1p5	(0x4D)
#define Voltage_ERM_OD_CL_1p8	(0x5D)
#define Voltage_ERM_OD_CL_2p0	(0x67)
#define Voltage_ERM_OD_CL_2p5	(0x81)
#define Voltage_ERM_OD_CL_2p7	(0x8B)
#define Voltage_ERM_OD_CL_3p0	(0x9B)
#define Voltage_ERM_OD_CL_3p3	(0xAA)
#define Voltage_ERM_OD_CL_3p6 	(0xBA)
#define Voltage_ERM_OD_CL_5p0	(0xFF)

// ERM Rated Voltage - Closed Loop
#define Voltage_ERM_RV_CL_1p3	(0x3D)
#define Voltage_ERM_RV_CL_1p5	(0x46)
#define Voltage_ERM_RV_CL_1p8	(0x54)
#define Voltage_ERM_RV_CL_2p0	(0x5E)
#define Voltage_ERM_RV_CL_2p5	(0x75)
#define Voltage_ERM_RV_CL_2p7	(0x7F)
#define Voltage_ERM_RV_CL_3p0	(0x8D)
#define Voltage_ERM_RV_CL_3p3	(0x9B)
#define Voltage_ERM_RV_CL_3p6 	(0xA9)
#define Voltage_ERM_RV_CL_5p0	(0xEB)

// LRA Rated Voltage - Closed Loop
#define VoltageRMS_LRA_RV_1p5	(0x3E)	// 1.5 Vrms
#define VoltageRMS_LRA_RV_2p0	(0x53)	// 2.0 Vrms

// Sequencer/ Waveform Related

#define	Waveform_SEQ1	(0x0F)
#define	Waveform_SEQ2	(0x10)
#define	Waveform_SEQ3	(0x11)
#define	Waveform_SEQ4	(0x12)
#define	Waveform_SEQ5	(0x13)
#define	Waveform_SEQ6	(0x14)
#define	Waveform_SEQ7	(0x15)
#define	Waveform_SEQ8	(0x16)

// LRA

#define LRA_Option1 	(0x2C)

#define LRA_Auto_OpenLoop_EN	(0x80)
#define LRA_Auto_OpenLoop_DIS	(0x00)

#define LRA_Waveshape_Sine		(0x01)  //Open Loop Mode
#define LRA_Waveshape_Square	(0x00)	//Open Loop Mode


// Open Loop LRA Period Group

#define LRA_OL_PeriodH		(0x2E)
#define LRA_OL_PeriodL		(0x2F)

//
#define LRA_Option			(0x27)
#define LRA_MIN_FRQ_125		(0x00)
#define LRA_MIN_FRQ_45		(0x80)
#define LRA_Resync_MIN		(0x00)
#define LRA_Resync_DrvTime	(0x40)

