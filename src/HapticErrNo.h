/*
 * HapticErrNo.h
 *
 *  Created on: Mar 21, 2014
 *      Author: a0220410
 */

#ifndef DRV2625EVM_CT_HAPTICERRNO_H_
#define DRV2625EVM_CT_HAPTICERRNO_H_

typedef enum HapticErrNo_t{
	NO_ERROR							=	0x0000,
	I2C_SENDSINGLE_FAIL					=	0x0001,
	I2C_NO_ACK							=	0x0002,
	I2C_RECV_UNCOMPLETE					=	0x0003,
	I2C_SENDMULTI_START_FAIL			=	0x0004,
	I2C_SEND_UNCOMPLETE					=	0x0005,
	INVALID_PARAMETER					=	0x0006,
	BUFFER_OVERFLOW						=	0x0007,
	I2C_ARBITRATION_LOST				=	0x0008,
	I2C_NO_DATA							=	0x0009,
	UNKNOWN_REPORT						=	0x7fff
}HapticErrNo;


#endif /* DRV2625EVM_CT_HAPTICERRNO_H_ */
