/*
 * INA231_EEPR.h
 *
 *  Created on: Mar 19, 2015
 *      Author: a0221097
 */
#ifndef INA231_EEPR_H_
#define INA231_EEPR_H_

#include <stdint.h>

/****************************************************************************/
/* INA231 Register Map*/  //Functional Name: INA
/****************************************************************************/
#define	Shunt_Voltage			(0x01)
#define Bus_Voltage				(0x02)
#define Power_Register			(0x03)
#define Current_Register		(0x04)
#define Calibration_Register	(0x05)

#define Mask_Enable				(0x06)
//// Mask Enable Registers///
#define Shunt_Over_Voltage		(0x8000)
#define Shunt_Under_Voltage		(0x4000)
#define Bus_Over_Voltage		(0x2000)
#define Bus_Under_Voltage		(0x1000)
#define Power_Over_Limit		(0x0800)
#define Conversion_Ready		(0x0400) //bit10
#define Alert_Function_Flag		(0x0010) //bit4
#define Conversion_Ready_Flag	(0x0008)
#define Math_Overflow_Flag		(0x0004)
#define	Alert_Polarity			(0x0002)
#define Alert_Latch_EN			(0x0001)
#define Alert_Latch_DIS			(0x0000)
#define	INA231_I2CADDR			0x40
int INA231_RegWrite(uint8_t reg, uint16_t val);
int INA231_RegRead(uint8_t reg);
/****************************************************************************/
/* EEPROM Register Map*/  //Functional Name: EEPR
/****************************************************************************/

//MEMORY- EEPROM
#define EEPROM_ADDR 	(0xA0)


#endif /* INA231_EEPR_H_ */
