/*
 * board.h
 *
 *  Created on: Apr 1, 2014
 *      Author: a0220410
 */

#ifndef BOARD_H_
#define BOARD_H_

#include "board_config.h"
#include "board_gpio.h"
#include "board_timer.h"
#include "board_spi.h"
#include "board_uart.h"
#include "LED.h"
#include "../INA231_EEPR.h"
#include "board_I2C.h"

#define	FW_VER_H	'1'
#define	FW_VER_L	'9'

void board_init();


#endif /* BOARD_H_ */
