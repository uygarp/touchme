/*
 * board_timer.h
 *
 *  Created on: Apr 1, 2014
 *      Author: a0220410
 */

#ifndef BOARD_TIMER_H_
#define BOARD_TIMER_H_

#include "driverlib.h"

#define	PWM_CLOCKSOURCE_DIVIDER		TIMER_A_CLOCKSOURCE_DIVIDER_16
#define PWM_NUMOFCOUNTER			(8000000/(2*PWM_CLOCKSOURCE_DIVIDER))

int DualPWMConfig(uint16_t freq, uint16_t duty, uint8_t sw, uint16_t *pErrNo);

void board_timer_init();

void timerdelay(unsigned int tdelay);
void sleep(unsigned int time);
#endif /* BOARD_TIMER_H_ */
