/*
 * board_i2c.c
 *
 *  Created on: Apr 3, 2014
 *      Author: a0220410
 */
#include "driverlib.h"
#include "board_I2C.h"
void initI2C()
{
    //Assign I2C pins to USCI_B1
    GPIO_setAsPeripheralModuleFunctionInputPin(
            GPIO_PORT_P4,
            GPIO_PIN1 + GPIO_PIN2
            );

    //Initialize Master
    USCI_B_I2C_masterInit(USCI_B1_BASE,
                          USCI_B_I2C_CLOCKSOURCE_SMCLK,
                          UCS_getSMCLK(),
                          USCI_B_I2C_SET_DATA_RATE_400KBPS
                          );

    USCI_B_I2C_enable(USCI_B1_BASE);
}

