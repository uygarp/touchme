/*
 * board_uart.c
 *
 *  Created on: Apr 7, 2014
 *      Author: a0220410
 */
#include "driverlib.h"
#include "board_uart.h"
#include "board_config.h"

uint8_t pBuffUartRx[UART_RTX_BUF_LEN];
uint8_t nUartReceiveCount;
uint8_t nUartReceiveLen;

void board_uart_init()
{
     //P4.4,5 = USCI_A1 TXD/RXD
        GPIO_setAsPeripheralModuleFunctionInputPin(
                GPIO_PORT_P4,
                GPIO_PIN4 + GPIO_PIN5
                );

        //Baudrate = 9600, clock freq = 8MHz
        //UCBRx = 52, UCBRFx = 1, UCBRSx = 0, UCOS16 = 1
        USCI_A_UART_initAdvance(USCI_A1_BASE,
								USCI_A_UART_CLOCKSOURCE_SMCLK,
								52,
								1,
								0,
								USCI_A_UART_EVEN_PARITY,
								USCI_A_UART_LSB_FIRST,
								USCI_A_UART_ONE_STOP_BIT,
								USCI_A_UART_MODE,
								USCI_A_UART_OVERSAMPLING_BAUDRATE_GENERATION );

        USCI_A_UART_enable(USCI_A1_BASE);
}

int uart_SendData(uint32_t count, uint8_t *pData)
{
	uint32_t i = 0;

	for(i = 0; i < count; i++){
		USCI_A_UART_transmitData(USCI_A1_BASE, pData[i]);
	}

    while(!USCI_A_UART_getInterruptStatus(USCI_A1_BASE, USCI_A_UART_TRANSMIT_INTERRUPT_FLAG));

    return count;
}

int uart_ReadData(uint32_t count, uint8_t *pData)
{
	uint32_t timeout = UART_RX_TIMEOUT;
	nUartReceiveCount = 0;
	nUartReceiveLen = count;

	USCI_A_UART_clearInterruptFlag(USCI_A1_BASE, USCI_A_UART_RECEIVE_INTERRUPT);
	USCI_A_UART_enableInterrupt(USCI_A1_BASE, USCI_A_UART_RECEIVE_INTERRUPT);

	while(timeout!= 0){
		if(USCI_A_UART_queryStatusFlags(USCI_A1_BASE, USCI_A_UART_BUSY)){
			timeout = UART_RX_TIMEOUT;
			continue;
		}else{
			timeout--;
		}
	}
	nUartReceiveLen = 0;
    return nUartReceiveCount;
}

//******************************************************************************
//
//This is the USCI_A0 interrupt vector service routine.
//
//******************************************************************************
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=USCI_A1_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(USCI_A1_VECTOR)))
#endif
void USCI_A1_ISR(void)
{
        switch (__even_in_range(UCA1IV, 4)) {
        //Vector 2 - RXIFG
        case 2:
        	if((nUartReceiveCount < UART_RTX_BUF_LEN)&&(nUartReceiveCount < nUartReceiveLen))
        		pBuffUartRx[nUartReceiveCount++] = USCI_A_UART_receiveData(USCI_A1_BASE);
        	else{
        		USCI_A_UART_clearInterruptFlag(USCI_A1_BASE, USCI_A_UART_RECEIVE_INTERRUPT);
        		USCI_A_UART_disableInterrupt(USCI_A1_BASE, USCI_A_UART_RECEIVE_INTERRUPT);
        	}
            break;
        default: break;
        }
}

