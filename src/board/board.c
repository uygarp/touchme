/*
 * board.c
 *
 *  Created on: Apr 1, 2014
 *      Author: a0220410
 */

#include "board.h"
#include "board_uart.h"

void board_init()
{

	initI2C();
	board_GPIO_config();

	GPIO_setOutputLowOnPin(LED_PORT_M, LED_MODEALL);
	GPIO_setOutputLowOnPin(LED_PORT_D, LED_DALL);
	GPIO_setOutputLowOnPin(LED_PORT_PM, LED_DM);
	GPIO_setOutputLowOnPin(LED_PORT_PM, LED_DP);
	GPIO_setOutputLowOnPin(GPIO_PORT_P1,GPIO_PIN3);

	GPIO_setOutputHighOnPin(EEPR_WPT);

	//P50
	GPIO_setAsOutputPin(GPIO_PORT_P5, GPIO_PIN0);
	GPIO_setOutputHighOnPin(GPIO_PORT_P5, GPIO_PIN0); //Highz to charge the LDO for startup
	timerdelay(2); //delay for a while
	GPIO_setAsInputPin(GPIO_PORT_P5, GPIO_PIN0);

#ifdef BOARD_UART
	//board_uart_init();
#endif
}

