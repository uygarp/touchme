/*
 * board_spi.c
 *
 *  Created on: Apr 3, 2014
 *      Author: a0220410
 */

#include "board_spi.h"
#include "board_config.h"
#include "driverlib.h"

void board_spi_init(){

	//P3.5,4,0 option select
	GPIO_setAsPeripheralModuleFunctionInputPin(
			GPIO_PORT_P4,
			GPIO_PIN5 + GPIO_PIN4 + GPIO_PIN3 + GPIO_PIN0
			);

	//Initialize Master
	USCI_A_SPI_masterInit(USCI_A1_BASE,
										USCI_A_SPI_CLOCKSOURCE_SMCLK,
										UCS_getSMCLK(),
										SPICLK,
										USCI_A_SPI_MSB_FIRST,
										USCI_A_SPI_PHASE_DATA_CHANGED_ONFIRST_CAPTURED_ON_NEXT,
										USCI_A_SPI_CLOCKPOLARITY_INACTIVITY_HIGH
										);

	//Enable SPI module
	//USCI_A_SPI_enable(USCI_A1_BASE);
}
