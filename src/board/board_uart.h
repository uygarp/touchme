/*
 * board_uart.h
 *
 *  Created on: Apr 7, 2014
 *      Author: a0220410
 */

#ifndef BOARD_UART_H_
#define BOARD_UART_H_

#include "stdint.h"

//#define BOARD_UART 1

void board_uart_init();
int uart_SendData(uint32_t count, uint8_t *pData);
int uart_ReadData(uint32_t count, uint8_t *pData);

#endif /* BOARD_UART_H_ */
