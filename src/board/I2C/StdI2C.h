/*
 * StdI2C.h
 *
 *  Created on: Mar 10, 2014
 *      Author: a0220410
 */

#ifndef STDI2C_H_
#define STDI2C_H_

#include <stdint.h>

extern uint8_t pBuffStdI2CRx[];
extern uint8_t pBuffStdI2CTx[];

int StdI2C_RX_Single(uint8_t SLAVE_ADDRESS, uint8_t reg, uint8_t *pVal,uint16_t *pErrNo);
int StdI2C_RX_Multiple(uint8_t SLAVE_ADDRESS, uint8_t reg, uint8_t len, uint8_t *pData, uint16_t *pErrNo);
int StdI2C_TX_Single(uint8_t SLAVE_ADDRESS, uint8_t reg, uint8_t val, uint16_t *pErrNo);
int StdI2C_TX_Multiple(uint8_t SLAVE_ADDRESS, uint8_t reg, uint8_t len, uint8_t *pData, uint16_t *pErrNo);
int FIFOI2C_TX_Multiple(uint8_t SLAVE_ADDRESS, uint8_t reg, uint8_t len, uint8_t *pData, uint16_t *pErrNo);
int FIFOI2C_RX_Multiple(uint8_t SLAVE_ADDRESS, uint8_t reg, uint8_t len, uint8_t *pData, uint16_t *pErrNo);
#endif /* STDI2C_H_ */
