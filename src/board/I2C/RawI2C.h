/*
 * RawI2C.h
 *
 *  Created on: Jul 31, 2014
 *      Author: a0220410
 */

#ifndef RAWI2C_H_
#define RAWI2C_H_

#include <stdint.h>

int RawI2C_TX_Single(uint8_t SLAVE_ADDRESS, uint8_t Data, uint16_t *pErrNo);
int RawI2C_TX_Multiple(uint8_t SLAVE_ADDRESS, uint8_t len, uint8_t *pData, uint16_t *pErrNo);
int RawI2C_RX_Single(uint8_t SLAVE_ADDRESS, uint8_t *pVal, uint16_t *pErrNo);
int RawI2C_RX_Multiple(uint8_t SLAVE_ADDRESS, uint8_t len, uint8_t *pData, uint16_t *pErrNo);

extern uint8_t *pDataBuf;

#endif /* RAWI2C_H_ */
