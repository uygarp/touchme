/*
 * I2CIsr.c
 *
 *  Created on: Mar 10, 2014
 *      Author: a0220410
 */

//******************************************************************************
//
//This is the USCI_B1 interrupt vector service routine.
//
//******************************************************************************
#include "driverlib.h"
#include "I2CCommon.h"
#include "StdI2C.h"
#include "RawI2C.h"
#include "I2CIsr.h"

volatile uint8_t nI2CRTXMode = 0;
volatile uint8_t nI2CRTRecieved = 0;
volatile uint8_t nI2CRTXCounter = 0;
volatile uint8_t nI2CRTXLength = 0;
volatile uint8_t nI2CRTSent = 0;
volatile uint8_t nI2CRTstatus = 0;

#pragma vector=USCI_B1_VECTOR
__interrupt void USCI_B1_ISR(void)
{
	int irqid = __even_in_range(UCB1IV, 12);
	switch (irqid) {
		case USCI_I2C_UCNACKIFG:
		{
			nI2CRTstatus = USCI_B_I2C_NAK_INTERRUPT;
			if((nI2CRTXMode == I2C_MODE_TX_MULTIPLE)
				||(nI2CRTXMode == I2C_MODE_RAWTX_MULTIPLE)){
				USCI_B_I2C_masterMultiByteSendStop(USCI_B1_BASE);
				__bic_SR_register_on_exit(LPM0_bits);
			}
			else if(nI2CRTXMode == I2C_MODE_RAWRX_MULTIPLE){
				USCI_B_I2C_masterMultiByteReceiveStop(USCI_B1_BASE);
				__bic_SR_register_on_exit(LPM0_bits);
			}
			else if((nI2CRTXMode == I2C_MODE_RX_SINGLE)
				||(nI2CRTXMode == I2C_MODE_RX_MULTIPLE)){
				USCI_B_I2C_masterMultiByteReceiveStop(USCI_B1_BASE);
			}else if(nI2CRTXMode == I2C_MODE_RAWRX_SINGLE){

			}
			break;
		}

		case USCI_I2C_UCTXIFG:
		{
			nI2CRTstatus = USCI_I2C_UCTXIFG;
			if(nI2CRTXMode == I2C_MODE_TX_MULTIPLE)
			{
				/* tx multiple */
				//Check TX byte counter
				if (nI2CRTXCounter < nI2CRTXLength) {
						USCI_B_I2C_masterMultiByteSendNext(USCI_B1_BASE,
								pBuffStdI2CTx[nI2CRTXCounter]);

						//Increment TX byte counter
						nI2CRTXCounter++;
				} else {
						//Initiate stop only
						USCI_B_I2C_masterMultiByteSendStop(USCI_B1_BASE);

						//Clear master interrupt status
						USCI_B_I2C_clearInterruptFlag(USCI_B1_BASE,
													  USCI_B_I2C_TRANSMIT_INTERRUPT);

						//Exit low power mode 0 and disable GIE on interrupt exit
						__bic_SR_register_on_exit(LPM0_bits);
						nI2CRTSent = 1;
				}
			}else if(nI2CRTXMode == I2C_MODE_RAWTX_MULTIPLE){
				//Check TX byte counter
				if (nI2CRTXCounter < nI2CRTXLength) {
						USCI_B_I2C_masterMultiByteSendNext(USCI_B1_BASE,
								pDataBuf[nI2CRTXCounter]);

						//Increment TX byte counter
						nI2CRTXCounter++;
				} else {
						//Initiate stop only
						USCI_B_I2C_masterMultiByteSendStop(USCI_B1_BASE);

						//Clear master interrupt status
						USCI_B_I2C_clearInterruptFlag(USCI_B1_BASE,
													  USCI_B_I2C_TRANSMIT_INTERRUPT);

						//Exit low power mode 0 and disable GIE on interrupt exit
						__bic_SR_register_on_exit(LPM0_bits);
						nI2CRTSent = 1;
				}
			}
			break;
		}

        case USCI_I2C_UCRXIFG:
        {
			/* tx single */
        	nI2CRTstatus = USCI_I2C_UCRXIFG;
			if(nI2CRTXMode == I2C_MODE_RX_SINGLE)
			{
                /*rx single*/
                //Grab data from data register
				pBuffStdI2CRx[0] = USCI_B_I2C_masterSingleReceive(
                        USCI_B1_BASE);
				nI2CRTRecieved = 1;
                //Exit low power mode 0 and disable GIE on interrupt exit
                __bic_SR_register_on_exit(LPM0_bits);
			}
			else if(nI2CRTXMode == I2C_MODE_RAWRX_SINGLE){
				*pDataBuf = USCI_B_I2C_masterSingleReceive(USCI_B1_BASE);
				nI2CRTRecieved = 1;
                //Exit low power mode 0 and disable GIE on interrupt exit
                __bic_SR_register_on_exit(LPM0_bits);
			}
			else if(nI2CRTXMode == I2C_MODE_RAWRX_MULTIPLE)
			{
				//Decrement RX byte counter
				/* rx multiple */
				if (nI2CRTXCounter < nI2CRTXLength)
				{
					if (nI2CRTXCounter == (nI2CRTXLength - 2)) {
							//Initiate end of reception -> Receive byte with NAK
						pDataBuf[nI2CRTXCounter] = USCI_B_I2C_masterMultiByteReceiveFinish(USCI_B1_BASE);
					}
					else
					{
						//Keep receiving one byte at a time
						pDataBuf[nI2CRTXCounter] = USCI_B_I2C_masterMultiByteReceiveNext(USCI_B1_BASE);
						if(nI2CRTXCounter == (nI2CRTXLength - 1)){
							nI2CRTRecieved = 1;
							__bic_SR_register_on_exit(LPM0_bits);
						}
					}

					nI2CRTXCounter++;
				}
			}
			else if(nI2CRTXMode == I2C_MODE_RX_MULTIPLE)
			{
                //Decrement RX byte counter
        		/* rx multiple */
                if (nI2CRTXCounter < nI2CRTXLength)
                {
					if (nI2CRTXCounter == (nI2CRTXLength - 2)) {
							//Initiate end of reception -> Receive byte with NAK
						pBuffStdI2CRx[nI2CRTXCounter] = USCI_B_I2C_masterMultiByteReceiveFinish(USCI_B1_BASE);
					}
					else
					{
						//Keep receiving one byte at a time
						pBuffStdI2CRx[nI2CRTXCounter] = USCI_B_I2C_masterMultiByteReceiveNext(USCI_B1_BASE);
						if(nI2CRTXCounter == (nI2CRTXLength - 1)){
							nI2CRTRecieved = 1;
							__bic_SR_register_on_exit(LPM0_bits);
						}
					}

					nI2CRTXCounter++;
                }
        	}
    		break;
		}

        default:
        	break;
    }
}
