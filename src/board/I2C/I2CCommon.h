/*
 * I2CCommon.h
 *
 *  Created on: Jul 31, 2014
 *      Author: a0220410
 */

#ifndef I2CCOMMON_H_
#define I2CCOMMON_H_

#define	I2C_MODE_RTX_IDLE		0x00
#define	I2C_MODE_TX_SINGLE		0x01
#define	I2C_MODE_RX_SINGLE		0x02
#define	I2C_MODE_TX_MULTIPLE	0x04
#define	I2C_MODE_RX_MULTIPLE	0x08
#define	I2C_MODE_RAWTX_SINGLE	0x10
#define	I2C_MODE_RAWTX_MULTIPLE	0x20
#define	I2C_MODE_RAWRX_SINGLE	0x40
#define	I2C_MODE_RAWRX_MULTIPLE	0x80

#define	I2C_RTX_TIMEOUT		1000

void clrI2CInterrupts(void);
void setDeviceAddress(uint8_t SLAVE_ADDRESS);
#endif /* I2CCOMMON_H_ */
