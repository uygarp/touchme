/*
 * I2CCommon.c
 *
 *  Created on: Jul 31, 2014
 *      Author: a0220410
 */
#include "driverlib.h"
#include "I2CCommon.h"
#include "I2CIsr.h"

static uint8_t slave_addr = 0;

void setDeviceAddress(uint8_t SLAVE_ADDRESS){
	if(slave_addr != SLAVE_ADDRESS){
		 USCI_B_I2C_setSlaveAddress(USCI_B1_BASE, SLAVE_ADDRESS);
		 slave_addr = SLAVE_ADDRESS;
	}
}

void clrI2CInterrupts(void){

	uint8_t irqMask = USCI_B_I2C_STOP_INTERRUPT
    		|USCI_B_I2C_START_INTERRUPT
    		|USCI_B_I2C_RECEIVE_INTERRUPT
    		|USCI_B_I2C_TRANSMIT_INTERRUPT
    		|USCI_B_I2C_NAK_INTERRUPT
    		|USCI_B_I2C_ARBITRATIONLOST_INTERRUPT;

	nI2CRTstatus = 0;
    USCI_B_I2C_clearInterruptFlag(USCI_B1_BASE,irqMask);
    USCI_B_I2C_disableInterrupt(USCI_B1_BASE, irqMask);
}
