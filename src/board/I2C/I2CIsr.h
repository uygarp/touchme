/*
 * I2CIsr.h
 *
 *  Created on: Jul 31, 2014
 *      Author: a0220410
 */

#ifndef I2CISR_H_
#define I2CISR_H_

#include <stdint.h>

extern volatile uint8_t nI2CRTXMode ;
extern volatile uint8_t nI2CRTRecieved;
extern volatile uint8_t nI2CRTXCounter;
extern volatile uint8_t nI2CRTXLength;
extern volatile uint8_t nI2CRTSent;
extern volatile uint8_t nI2CRTstatus;

#endif /* I2CISR_H_ */
