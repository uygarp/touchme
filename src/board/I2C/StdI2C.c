/*
 * StdI2C.c
 *
 *  Created on: Mar 10, 2014
 *      Author: a0220410
 */
#include "driverlib.h"
#include "I2CCommon.h"
#include "I2CIsr.h"
#include "StdI2C.h"
#include "HapticErrNo.h"
#include "board_config.h"

uint8_t pBuffStdI2CRx[I2C_RTX_BUF_LEN];
uint8_t pBuffStdI2CTx[I2C_RTX_BUF_LEN];

uint8_t i2cstatus;
uint8_t fifotemp[400];
uint16_t fifotempcount = 0;

int StdI2C_TX_Single(uint8_t SLAVE_ADDRESS, uint8_t reg, uint8_t val, uint16_t *pErrNo){

	return StdI2C_TX_Multiple(SLAVE_ADDRESS, reg, 1, &val, pErrNo);
}

int StdI2C_RX_Single(uint8_t SLAVE_ADDRESS, uint8_t reg, uint8_t *pVal,uint16_t *pErrNo){
	int read_bytes = -1;

	setDeviceAddress(SLAVE_ADDRESS);

    USCI_B_I2C_setMode(USCI_B1_BASE,
                       USCI_B_I2C_TRANSMIT_MODE
                       );

    nI2CRTstatus = 0;
    nI2CRTXMode = I2C_MODE_RX_SINGLE;

    clrI2CInterrupts();
    USCI_B_I2C_enableInterrupt(USCI_B1_BASE, USCI_B_I2C_NAK_INTERRUPT|USCI_B_I2C_ARBITRATIONLOST_INTERRUPT);

    bool bSuc = USCI_B_I2C_masterSendSingleByteWithTimeout(USCI_B1_BASE, reg, I2C_RTX_TIMEOUT);
    if(nI2CRTstatus == USCI_B_I2C_NAK_INTERRUPT){
    	clrI2CInterrupts();
    	*pErrNo = I2C_NO_ACK;
    	return -2;
    }

    if(bSuc == STATUS_FAIL){
    	clrI2CInterrupts();
    	//USCI_B_I2C_masterMultiByteReceiveStop(USCI_B1_BASE);
    	*pErrNo = I2C_SENDSINGLE_FAIL;
    	return -1;
    }

    while (USCI_B_I2C_isBusBusy(USCI_B1_BASE)) ;

    USCI_B_I2C_setMode(USCI_B1_BASE,
                       USCI_B_I2C_RECEIVE_MODE
                       );

    clrI2CInterrupts();

    nI2CRTRecieved = 0;
    nI2CRTstatus = 0;
    USCI_B_I2C_enableInterrupt(USCI_B1_BASE,
    		USCI_B_I2C_RECEIVE_INTERRUPT
                                );

    USCI_B_I2C_masterSingleReceiveStart(USCI_B1_BASE);

    //Enter low power mode 0 with interrupts enabled.
    __bis_SR_register(LPM0_bits + GIE);

    while (USCI_B_I2C_isBusBusy(USCI_B1_BASE)) ;

    clrI2CInterrupts();

    if(nI2CRTRecieved == 1){
    	read_bytes = 1;
    	*pVal = pBuffStdI2CRx[0];
    	*pErrNo = NO_ERROR;
    }else{
    	*pErrNo = I2C_RECV_UNCOMPLETE;
    }

    nI2CRTXMode = I2C_MODE_RTX_IDLE;
    nI2CRTRecieved = 0;
    return read_bytes;
}

int StdI2C_TX_Multiple(uint8_t SLAVE_ADDRESS, uint8_t reg, uint8_t len, uint8_t *pData, uint16_t *pErrNo){

	int sendBytes = -1;

	setDeviceAddress(SLAVE_ADDRESS);

	USCI_B_I2C_setMode(USCI_B1_BASE,
                       USCI_B_I2C_TRANSMIT_MODE
                       );

    pBuffStdI2CTx[0] = reg;
    memcpy(&pBuffStdI2CTx[1], pData, len);

    clrI2CInterrupts();

	nI2CRTXMode = I2C_MODE_TX_MULTIPLE;
	nI2CRTSent = 0;
    nI2CRTXCounter = 1;
    nI2CRTXLength = len+1;
    nI2CRTstatus = 0;
	USCI_B_I2C_enableInterrupt(USCI_B1_BASE,
							   USCI_B_I2C_TRANSMIT_INTERRUPT|USCI_B_I2C_NAK_INTERRUPT|USCI_B_I2C_ARBITRATIONLOST_INTERRUPT
							   );

	//Initiate start and send first character
	bool bSuc = USCI_B_I2C_masterMultiByteSendStartWithTimeout(USCI_B1_BASE,
			pBuffStdI2CTx[0], I2C_RTX_TIMEOUT);
	if(bSuc == STATUS_FAIL){
		nI2CRTXMode = I2C_MODE_RTX_IDLE;
		nI2CRTSent = 0;
		USCI_B_I2C_masterMultiByteSendStop(USCI_B1_BASE);
		clrI2CInterrupts();
		*pErrNo = I2C_SENDMULTI_START_FAIL;
		return -2;
	}

   // while (nI2CRTstatus == 0) ;
	//Enter LPM0 with interrupts enabled
	__bis_SR_register(LPM0_bits + GIE);

	if(nI2CRTstatus == USCI_B_I2C_NAK_INTERRUPT){
    	nI2CRTXMode = I2C_MODE_RTX_IDLE;
    	nI2CRTSent = 0;
    	clrI2CInterrupts();
    	*pErrNo = I2C_NO_ACK;
    	return -3;
    }
    clrI2CInterrupts();

    if(nI2CRTSent == 1){
    	sendBytes = nI2CRTXCounter - 1;
    	*pErrNo = NO_ERROR;
    }else{
    	*pErrNo = I2C_SEND_UNCOMPLETE;
    }

    nI2CRTXMode = I2C_MODE_RTX_IDLE;
    nI2CRTSent = 0;

    return sendBytes;
}

/****************************************************************************
  I2C communication for DVR2667 FIFO PLAYBACK MODE

  Date: 7/10/2014
  By Kevin Zhang
 ****************************************************************************/

int FIFOI2C_TX_Multiple(uint8_t SLAVE_ADDRESS, uint8_t reg, uint8_t len, uint8_t *pData, uint16_t *pErrNo){

	setDeviceAddress(SLAVE_ADDRESS);

	USCI_B_I2C_setMode(USCI_B1_BASE,USCI_B_I2C_TRANSMIT_MODE);
    pBuffStdI2CTx[0] = reg;
    memcpy(&pBuffStdI2CTx[1], pData, len);

    nI2CRTXCounter = 1;
    nI2CRTXLength = len+1;
    nI2CRTstatus = 0;

	//Initiate start and send first character
	bool bSuc = USCI_B_I2C_masterMultiByteSendStartWithTimeout(USCI_B1_BASE,pBuffStdI2CTx[0], I2C_RTX_TIMEOUT);

	while(1)
    {
    	i2cstatus = USCI_B_I2C_getInterruptStatus(USCI_B1_BASE, USCI_B_I2C_TRANSMIT_INTERRUPT + USCI_B_I2C_NAK_INTERRUPT);
    	switch (i2cstatus)
    	{
			//No ack mode
    		case USCI_B_I2C_NAK_INTERRUPT:
    		{
    			nI2CRTstatus = USCI_I2C_UCNACKIFG;
				//send stop after Noack to restart the progress
				USCI_B_I2C_masterMultiByteSendStop(USCI_B1_BASE);
				*pErrNo = I2C_SEND_UNCOMPLETE;
				return nI2CRTXCounter - 1;
    		}
			
			//Transmit mode
    		case USCI_B_I2C_TRANSMIT_INTERRUPT:
    		{
    			nI2CRTstatus = USCI_I2C_UCTXIFG;
    			//Check TX byte counter
    			if (nI2CRTXCounter < nI2CRTXLength)
    			{
    				USCI_B_I2C_masterMultiByteSendNext(USCI_B1_BASE,pBuffStdI2CTx[nI2CRTXCounter]);
					//Increment TX byte counter
    				if((pBuffStdI2CTx[nI2CRTXCounter] == 0) && (pBuffStdI2CTx[nI2CRTXCounter+1] == 0))
    					nI2CRTXCounter += 3;
    				else
    					nI2CRTXCounter++;
/*
    				if(fifotempcount<=400)
    				{
    					fifotemp[fifotempcount] = pBuffStdI2CTx[nI2CRTXCounter-1];
 						fifotempcount++;
    				}
*/
    			}
    			else
    			{
    			//Initiate stop only
   					USCI_B_I2C_masterMultiByteSendStop(USCI_B1_BASE);
    			    *pErrNo = NO_ERROR;
 					return nI2CRTXCounter - 1;
    				}
    			break;
    		}

            default:
            	break;
        }
    }

}

int StdI2C_RX_Multiple(uint8_t SLAVE_ADDRESS, uint8_t reg, uint8_t len, uint8_t *pData, uint16_t *pErrNo){

	int readBytes = -1;

	if(len == 1){
		*pErrNo = INVALID_PARAMETER;
		return 0;
	}

	setDeviceAddress(SLAVE_ADDRESS);

    USCI_B_I2C_setMode(USCI_B1_BASE,
                       USCI_B_I2C_TRANSMIT_MODE
                       );

    nI2CRTstatus = 0;
    nI2CRTXMode = I2C_MODE_RX_MULTIPLE;

    clrI2CInterrupts();
    USCI_B_I2C_enableInterrupt(USCI_B1_BASE, USCI_B_I2C_NAK_INTERRUPT|USCI_B_I2C_ARBITRATIONLOST_INTERRUPT);
    //Enable I2C Module to start operations
    bool bSuc = USCI_B_I2C_masterSendSingleByteWithTimeout(USCI_B1_BASE, reg, I2C_RTX_TIMEOUT);
    if(nI2CRTstatus == USCI_B_I2C_NAK_INTERRUPT){
    	clrI2CInterrupts();
    	*pErrNo = I2C_NO_ACK;
    	return -2;
    }

    if(bSuc == STATUS_FAIL){
    	clrI2CInterrupts();
    	*pErrNo = I2C_SENDSINGLE_FAIL;
    	return -1;
    }

    while (USCI_B_I2C_isBusBusy(USCI_B1_BASE)) ;

    //Set Master in receive mode
    USCI_B_I2C_setMode(USCI_B1_BASE,
                       USCI_B_I2C_RECEIVE_MODE
                       );

    clrI2CInterrupts();


    nI2CRTXLength = len;
    nI2CRTXCounter = 0;
    nI2CRTRecieved = 0;
    nI2CRTstatus = 0;
     //Enable master Receive interrupt
    USCI_B_I2C_enableInterrupt(USCI_B1_BASE,
                                USCI_B_I2C_RECEIVE_INTERRUPT
                                );

     //Initialize multi reception
	 USCI_B_I2C_masterMultiByteReceiveStart(USCI_B1_BASE);

	 //Enter low power mode 0 with interrupts enabled.
	 __bis_SR_register(LPM0_bits + GIE);

	while (USCI_B_I2C_isBusBusy(USCI_B1_BASE)) ;

	clrI2CInterrupts();

	nI2CRTXMode = I2C_MODE_RTX_IDLE;
	nI2CRTSent = 0;

	if(nI2CRTRecieved == 1){
		readBytes = len;
		memcpy(pData, pBuffStdI2CRx, len);
		*pErrNo = NO_ERROR;
	}else{
		*pErrNo = I2C_RECV_UNCOMPLETE;
	}

	return readBytes;
}


