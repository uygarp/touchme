/*
 * RawI2C.c
 *
 *  Created on: Jul 31, 2014
 *      Author: a0220410
 */
#include "driverlib.h"
#include "HapticErrNo.h"
#include "I2CCommon.h"
#include "I2CIsr.h"
#include "RawI2C.h"

uint8_t *pDataBuf = 0;

int RawI2C_TX_Single(uint8_t SLAVE_ADDRESS, uint8_t Data, uint16_t *pErrNo){

	uint8_t isrStatus = 0;

	setDeviceAddress(SLAVE_ADDRESS);

	USCI_B_I2C_setMode(USCI_B1_BASE,
                       USCI_B_I2C_TRANSMIT_MODE
                       );

    clrI2CInterrupts();

    USCI_B_I2C_masterSendSingleByteWithTimeout(USCI_B1_BASE, Data, I2C_RTX_TIMEOUT);

    isrStatus = USCI_B_I2C_getInterruptStatus(USCI_B1_BASE,  USCI_B_I2C_NAK_INTERRUPT|USCI_B_I2C_ARBITRATIONLOST_INTERRUPT);

	clrI2CInterrupts();

	if(isrStatus == 0){
		*pErrNo = NO_ERROR;
		return 1;
	}else if((isrStatus&USCI_B_I2C_NAK_INTERRUPT)==USCI_B_I2C_NAK_INTERRUPT){
    	*pErrNo = I2C_NO_ACK;
    	return -2;
    }else{
    	*pErrNo = I2C_ARBITRATION_LOST;
    	return -3;
    }
}

int RawI2C_TX_Multiple(uint8_t SLAVE_ADDRESS, uint8_t len, uint8_t *pData, uint16_t *pErrNo){

	int sendBytes = -1;

	setDeviceAddress(SLAVE_ADDRESS);

	USCI_B_I2C_setMode(USCI_B1_BASE,
                       USCI_B_I2C_TRANSMIT_MODE
                       );

    clrI2CInterrupts();

	nI2CRTXMode = I2C_MODE_RAWTX_MULTIPLE;
	nI2CRTXCounter = 1;
	nI2CRTXLength = len;
	nI2CRTSent = 0;
	nI2CRTstatus = 0;
	pDataBuf = pData;
	USCI_B_I2C_enableInterrupt(USCI_B1_BASE,
							   USCI_B_I2C_TRANSMIT_INTERRUPT|USCI_B_I2C_NAK_INTERRUPT|USCI_B_I2C_ARBITRATIONLOST_INTERRUPT
							   );

	//Initiate start and send first character
	bool bSuc = USCI_B_I2C_masterMultiByteSendStartWithTimeout(USCI_B1_BASE,
			pDataBuf[0], I2C_RTX_TIMEOUT);
	if(bSuc == STATUS_FAIL){
		nI2CRTXMode = I2C_MODE_RTX_IDLE;
		nI2CRTSent = 0;
		USCI_B_I2C_masterMultiByteSendStop(USCI_B1_BASE);
		clrI2CInterrupts();
		*pErrNo = I2C_SENDMULTI_START_FAIL;
		return -2;
	}

	__bis_SR_register(LPM0_bits + GIE);

    if(nI2CRTstatus == USCI_B_I2C_NAK_INTERRUPT){
    	clrI2CInterrupts();
		nI2CRTXMode = I2C_MODE_RTX_IDLE;
		nI2CRTSent = 0;
    	*pErrNo = I2C_NO_ACK;
    	return -4;
    }

	clrI2CInterrupts();
	nI2CRTXMode = I2C_MODE_RTX_IDLE;

    if(nI2CRTSent == 1){
    	sendBytes = nI2CRTXLength;
    	*pErrNo = NO_ERROR;
    }else{
    	*pErrNo = I2C_SEND_UNCOMPLETE;
    }

    nI2CRTSent = 0;
    return sendBytes;
}

int RawI2C_RX_Single(uint8_t SLAVE_ADDRESS, uint8_t *pVal, uint16_t *pErrNo){
	int read_bytes = -1;

	setDeviceAddress(SLAVE_ADDRESS);

    USCI_B_I2C_setMode(USCI_B1_BASE,
                       USCI_B_I2C_RECEIVE_MODE
                       );

    clrI2CInterrupts();
    pDataBuf = pVal;
    nI2CRTRecieved = 0;
    nI2CRTstatus = 0;
    nI2CRTXMode = I2C_MODE_RAWRX_SINGLE;

    USCI_B_I2C_enableInterrupt(USCI_B1_BASE,
    		USCI_B_I2C_RECEIVE_INTERRUPT|USCI_B_I2C_NAK_INTERRUPT|USCI_B_I2C_ARBITRATIONLOST_INTERRUPT
                                );

    USCI_B_I2C_masterSingleReceiveStart(USCI_B1_BASE);

    if(nI2CRTstatus == USCI_B_I2C_NAK_INTERRUPT){
    	clrI2CInterrupts();
		nI2CRTXMode = I2C_MODE_RTX_IDLE;
		nI2CRTRecieved = 0;
    	*pErrNo = I2C_NO_ACK;
    	return -3;
    }

    //Enter low power mode 0 with interrupts enabled.
    __bis_SR_register(LPM0_bits + GIE);

    clrI2CInterrupts();
    nI2CRTXMode = I2C_MODE_RTX_IDLE;

    if(nI2CRTRecieved == 1){
    	read_bytes = 1;
    	*pErrNo = NO_ERROR;
    }else{
    	*pErrNo = I2C_RECV_UNCOMPLETE;
    }
    nI2CRTRecieved = 0;
    return read_bytes;
}

int RawI2C_RX_Multiple(uint8_t SLAVE_ADDRESS, uint8_t len, uint8_t *pData, uint16_t *pErrNo){

	int readBytes = -1;

	if(len == 1){
		*pErrNo = INVALID_PARAMETER;
		return -2;
	}

	setDeviceAddress(SLAVE_ADDRESS);

    //Set Master in receive mode
    USCI_B_I2C_setMode(USCI_B1_BASE,
                       USCI_B_I2C_RECEIVE_MODE
                       );

    clrI2CInterrupts();
    nI2CRTXMode = I2C_MODE_RAWRX_MULTIPLE;
    nI2CRTXLength = len;
    nI2CRTXCounter = 0;
    nI2CRTRecieved = 0;
    nI2CRTstatus = 0;
    pDataBuf = pData;
     //Enable master Receive interrupt
    USCI_B_I2C_enableInterrupt(USCI_B1_BASE,
    		USCI_B_I2C_RECEIVE_INTERRUPT|USCI_B_I2C_NAK_INTERRUPT|USCI_B_I2C_ARBITRATIONLOST_INTERRUPT
                                );

	//Initialize multi reception
	USCI_B_I2C_masterMultiByteReceiveStart(USCI_B1_BASE);

	//Enter low power mode 0 with interrupts enabled.
	__bis_SR_register(LPM0_bits + GIE);

    if(nI2CRTstatus == USCI_B_I2C_NAK_INTERRUPT){
    	clrI2CInterrupts();
    	*pErrNo = I2C_NO_ACK;
    	return -3;
    }

	clrI2CInterrupts();

	nI2CRTXMode = I2C_MODE_RTX_IDLE;
	nI2CRTSent = 0;

	if(nI2CRTRecieved == 1){
		readBytes = len;
		*pErrNo = NO_ERROR;
	}else{
		*pErrNo = I2C_RECV_UNCOMPLETE;
	}

	return readBytes;
}
