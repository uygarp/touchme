/*
 * StdPollI2C.c
 *
 *  Created on: Aug 6, 2014
 *      Author: a0220410
 */
#include "driverlib.h"
#include "StdPollI2C.h"
#include "I2CCommon.h"
#include "HapticErrNo.h"

int StdI2C_P_RX_Single(uint8_t SLAVE_ADDRESS, uint8_t reg, uint8_t *pVal,uint16_t *pErrNo){
	uint8_t isrStatus = 0;

	setDeviceAddress(SLAVE_ADDRESS);

    USCI_B_I2C_setMode(USCI_B1_BASE, USCI_B_I2C_TRANSMIT_MODE);

    clrI2CInterrupts();

    USCI_B_I2C_masterSendSingleByteWithTimeout(USCI_B1_BASE, reg, I2C_RTX_TIMEOUT);
    isrStatus = USCI_B_I2C_getInterruptStatus(USCI_B1_BASE,  USCI_B_I2C_NAK_INTERRUPT|USCI_B_I2C_ARBITRATIONLOST_INTERRUPT);
    if((isrStatus & USCI_B_I2C_NAK_INTERRUPT) == USCI_B_I2C_NAK_INTERRUPT){
    	*pErrNo =  I2C_NO_ACK;
    	return -2;
    }else if((isrStatus & USCI_B_I2C_ARBITRATIONLOST_INTERRUPT) == USCI_B_I2C_ARBITRATIONLOST_INTERRUPT){
    	*pErrNo =  I2C_ARBITRATION_LOST;
    	return -3;
    }

    while (USCI_B_I2C_isBusBusy(USCI_B1_BASE)) ;

    USCI_B_I2C_setMode(USCI_B1_BASE, USCI_B_I2C_RECEIVE_MODE);
    clrI2CInterrupts();

    USCI_B_I2C_masterSingleReceiveStart(USCI_B1_BASE);

    while(1){
    	isrStatus = USCI_B_I2C_getInterruptStatus(USCI_B1_BASE,  USCI_B_I2C_RECEIVE_INTERRUPT);
    	    if(isrStatus > 0){
    	    	*pVal = USCI_B_I2C_masterSingleReceive(USCI_B1_BASE);
    	    	break;
    	    }
    }

    while (USCI_B_I2C_isBusBusy(USCI_B1_BASE)) ;

    clrI2CInterrupts();
    *pErrNo = NO_ERROR;

    return 1;
}

int StdI2C_P_TX_Single(uint8_t SLAVE_ADDRESS, uint8_t reg, uint8_t val, uint16_t *pErrNo){
	uint8_t isrStatus = 0;
	uint8_t sendFinish = 0;

	setDeviceAddress(SLAVE_ADDRESS);

	USCI_B_I2C_setMode(USCI_B1_BASE,
                       USCI_B_I2C_TRANSMIT_MODE
                       );

    clrI2CInterrupts();

	//Initiate start and send first character
    USCI_B_I2C_masterMultiByteSendStartWithTimeout(USCI_B1_BASE, reg, I2C_RTX_TIMEOUT);

    while(1){
		isrStatus = USCI_B_I2C_getInterruptStatus(USCI_B1_BASE,  USCI_B_I2C_NAK_INTERRUPT|USCI_B_I2C_ARBITRATIONLOST_INTERRUPT|USCI_B_I2C_TRANSMIT_INTERRUPT);
		if((isrStatus & USCI_B_I2C_NAK_INTERRUPT) == USCI_B_I2C_NAK_INTERRUPT){
			USCI_B_I2C_masterMultiByteSendStop(USCI_B1_BASE);
			*pErrNo =  I2C_NO_ACK;
			break;
		}else if((isrStatus & USCI_B_I2C_ARBITRATIONLOST_INTERRUPT) == USCI_B_I2C_ARBITRATIONLOST_INTERRUPT){
			USCI_B_I2C_masterMultiByteSendStop(USCI_B1_BASE);
			*pErrNo =  I2C_ARBITRATION_LOST;
			break;
		}else if((isrStatus & USCI_B_I2C_TRANSMIT_INTERRUPT) == USCI_B_I2C_TRANSMIT_INTERRUPT){
			if(sendFinish == 0){
				USCI_B_I2C_masterMultiByteSendNext(USCI_B1_BASE, val);
				*pErrNo = NO_ERROR;
				sendFinish = 1;
			}else{
				USCI_B_I2C_masterMultiByteSendStop(USCI_B1_BASE);
				break;
			}
		}
    }

    clrI2CInterrupts();

    return (sendFinish==1)?1:-1;
}

//Code by Kevin Zhang
//2014.8
int StdI2C_P_TX_Multiple(uint8_t SLAVE_ADDRESS, uint8_t reg, uint8_t len, uint8_t *pData, uint16_t *pErrNo)
{
	uint8_t isrStatus = 0;
	uint8_t sendFinish = 0;
	uint8_t sendCounter = 0;

	setDeviceAddress(SLAVE_ADDRESS);

	USCI_B_I2C_setMode(USCI_B1_BASE, USCI_B_I2C_TRANSMIT_MODE);

    clrI2CInterrupts();

	//Initiate start and send first character
	USCI_B_I2C_masterMultiByteSendStartWithTimeout(USCI_B1_BASE, reg, I2C_RTX_TIMEOUT);

	while(1){
		isrStatus = USCI_B_I2C_getInterruptStatus(USCI_B1_BASE,  USCI_B_I2C_NAK_INTERRUPT|USCI_B_I2C_ARBITRATIONLOST_INTERRUPT|USCI_B_I2C_TRANSMIT_INTERRUPT);
		if((isrStatus & USCI_B_I2C_NAK_INTERRUPT) == USCI_B_I2C_NAK_INTERRUPT){
			USCI_B_I2C_masterMultiByteSendStop(USCI_B1_BASE);
			*pErrNo =  I2C_NO_ACK;
			break;
		}else if((isrStatus & USCI_B_I2C_ARBITRATIONLOST_INTERRUPT) == USCI_B_I2C_ARBITRATIONLOST_INTERRUPT){
			USCI_B_I2C_masterMultiByteSendStop(USCI_B1_BASE);
			*pErrNo =  I2C_ARBITRATION_LOST;
			break;
		}else if((isrStatus & USCI_B_I2C_TRANSMIT_INTERRUPT) == USCI_B_I2C_TRANSMIT_INTERRUPT){
			if(sendFinish == 0)
			{
				USCI_B_I2C_masterMultiByteSendNext(USCI_B1_BASE, pData[sendCounter]);
				sendCounter++;
				if(sendCounter == len)
				{
					*pErrNo = NO_ERROR;
					sendFinish = 1;
				}
			}
			else
			{
				USCI_B_I2C_masterMultiByteSendStop(USCI_B1_BASE);
				break;
			}
		}
    }

    clrI2CInterrupts();

    return (sendFinish==1)?sendCounter:-1;
}

int StdI2C_P_RX_Multiple(uint8_t SLAVE_ADDRESS, uint8_t reg, uint8_t len, uint8_t *pData, uint16_t *pErrNo)
{
	uint8_t readLength = len;
	uint8_t isrStatus = 0;
	uint8_t readCounter = 0;
	uint8_t Recievedstatus = 0;

	setDeviceAddress(SLAVE_ADDRESS);

    USCI_B_I2C_setMode(USCI_B1_BASE, USCI_B_I2C_TRANSMIT_MODE);

    clrI2CInterrupts();

    USCI_B_I2C_masterSendSingleByteWithTimeout(USCI_B1_BASE, reg, I2C_RTX_TIMEOUT);

    isrStatus = USCI_B_I2C_getInterruptStatus(USCI_B1_BASE,  USCI_B_I2C_NAK_INTERRUPT|USCI_B_I2C_ARBITRATIONLOST_INTERRUPT);
    if((isrStatus & USCI_B_I2C_NAK_INTERRUPT) == USCI_B_I2C_NAK_INTERRUPT){
    	*pErrNo =  I2C_NO_ACK;
    	return -2;
    }else if((isrStatus & USCI_B_I2C_ARBITRATIONLOST_INTERRUPT) == USCI_B_I2C_ARBITRATIONLOST_INTERRUPT){
    	*pErrNo =  I2C_ARBITRATION_LOST;
    	return -3;
    }

    while (USCI_B_I2C_isBusBusy(USCI_B1_BASE)) ;

    //Set Master in receive mode
    USCI_B_I2C_setMode(USCI_B1_BASE, USCI_B_I2C_RECEIVE_MODE);

    clrI2CInterrupts();

     //Initialize multi reception
	USCI_B_I2C_masterMultiByteReceiveStart(USCI_B1_BASE);

    while(1){
    	isrStatus = USCI_B_I2C_getInterruptStatus(USCI_B1_BASE,  USCI_B_I2C_RECEIVE_INTERRUPT);
    	if(isrStatus > 0)
    	{
            if (readCounter < readLength)
            {
				if (readCounter == (readLength - 2)) {
					//Initiate end of reception -> Receive byte with NAK
					pData[readCounter] = USCI_B_I2C_masterMultiByteReceiveFinish(USCI_B1_BASE);
				}
				else
				{
					//Keep receiving one byte at a time
					pData[readCounter] = USCI_B_I2C_masterMultiByteReceiveNext(USCI_B1_BASE);
					if(readCounter == (readLength - 1))
					{
						Recievedstatus = 1;
						break;
					}
				}
				readCounter++;
            }
    	}
    }

	while (USCI_B_I2C_isBusBusy(USCI_B1_BASE)) ;

	clrI2CInterrupts();

	if(Recievedstatus == 1){
		*pErrNo = NO_ERROR;
	}else{
		*pErrNo = I2C_RECV_UNCOMPLETE;
		return -1;
	}

	return readLength;
}
//END
