/*
 * StdPollI2C.h
 *
 *  Created on: Aug 6, 2014
 *      Author: a0220410
 */

#ifndef STDPOLLI2C_H_
#define STDPOLLI2C_H_

#include <stdint.h>

int StdI2C_P_RX_Single(uint8_t SLAVE_ADDRESS, uint8_t reg, uint8_t *pVal,uint16_t *pErrNo);
int StdI2C_P_TX_Single(uint8_t SLAVE_ADDRESS, uint8_t reg, uint8_t val, uint16_t *pErrNo);
int StdI2C_P_RX_Multiple(uint8_t SLAVE_ADDRESS, uint8_t reg, uint8_t len, uint8_t *pData, uint16_t *pErrNo);
int StdI2C_P_TX_Multiple(uint8_t SLAVE_ADDRESS, uint8_t reg, uint8_t len, uint8_t *pData, uint16_t *pErrNo);

#endif /* STDPOLLI2C_H_ */
