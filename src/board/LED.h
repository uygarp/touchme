/*
 * LED.c
 *
 *  Created on: Mar 11, 2015
 *      Author: a0221097
 */
#ifndef LED_H_
#define LED_H_

#include "stdint.h"
#include<string.h>
#include "board_uart.h"
#define LEDBLINKDELAY 	1500

//Mode LEDS
#define LED_PORT_M  	GPIO_PORT_P2
#define LED_M0         	GPIO_PIN7
#define LED_M1         	GPIO_PIN4
#define LED_M2         	GPIO_PIN5
#define LED_M3         	GPIO_PIN6
#define LED_MODEALL    LED_M0 + LED_M1 + LED_M2 + LED_M3


//DPAD LEDS (also known as Wheel)
#define LED_PORT_D 		GPIO_PORT_P4
#define LED_D1			GPIO_PIN0
#define LED_D2         	GPIO_PIN3
#ifdef BOARD_UART
#define LED_D3         	LED_D1
#define LED_D4         	LED_D2
#else
#define LED_D3          GPIO_PIN4
#define LED_D4          GPIO_PIN5
#endif
#define LED_DT         	GPIO_PIN6
#define LED_DALL		LED_D1 + LED_D2 + LED_D3 + LED_D4 + LED_DT

#define LED_PORT_PM		GPIO_PORT_PJ
#define LED_DP			GPIO_PIN0
#define LED_DM			GPIO_PIN1
#define LED_PORT_PM_ALL LED_DP + LED_DM

void Board_FlashLED(unsigned int numberOfBlinks);
void LED_On(uint8_t LEDPort, uint8_t LED_ID);
void LED_Off(uint8_t LEDPort, uint8_t LED_ID);
void LED_Toggle(uint8_t LEDPort, uint8_t LED_ID);
void LED_Blink(uint8_t LEDPort, uint8_t LED_ID);
void LED_MODE(uint8_t mode);

#endif
