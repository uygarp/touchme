/*
 * gpio.c
 *
 *  Created on: Apr 1, 2014
 *      Author: a0220410
 */
#include "board_gpio.h"
#include "DRV2625.h"
#include "HapticErrNo.h"
#include "board.h"
#include "LED.h"

void board_GPIO_config()
{
	GPIO_setAsOutputPin(LED_PORT_M, LED_MODEALL);
	GPIO_setAsOutputPin(LED_PORT_D, LED_DALL);
	GPIO_setAsOutputPin(LED_PORT_PM, LED_DM);
	GPIO_setAsOutputPin(LED_PORT_PM, LED_DP);

	// Load Switch is Output
	GPIO_setAsOutputPin(DRV_Switch);
	GPIO_setAsOutputPin(LED_NRST);
	GPIO_setAsOutputPin(DRV_NRST);

	GPIO_setAsOutputPin(EEPR_WPT);

    GPIO_setAsOutputPin(TEST_OUTPUT0);
    GPIO_setAsOutputPin(TEST_OUTPUT1);

    GPIO_setAsInputPin(CC2650_NRST_STATE);
    GPIO_setAsInputPin(CC2650_LOAD_STATE);
    GPIO_setAsInputPin(CC2650_TRIG_STATE);

    //Enable P1.2 internal resistance as pull-Up resistance
    GPIO_setAsInputPinWithPullUpresistor(CC2650_INTERRUPT);

    //P1.2 interrupt enabled
    GPIO_enableInterrupt(CC2650_INTERRUPT);

    //P1.2 Hi/Lo edge
    GPIO_interruptEdgeSelect(CC2650_INTERRUPT, GPIO_HIGH_TO_LOW_TRANSITION);

    //P1.2 IFG cleared
    GPIO_clearInterruptFlag(CC2650_INTERRUPT);
}

int GPIO_Config(uint8_t Config, uint8_t Bank, uint16_t PinNum, uint16_t *pErrNum)
{
	int ret = 0;
	uint8_t mode = (Config&0xc0)>>6;
	switch(mode)
	{
		case 0:		//GPIO Input
		{
			uint8_t state = (Config&0x30)>>4;
			switch(state)
			{
				case 0:GPIO_setAsInputPin(Bank,PinNum);break;
				case 1:GPIO_setAsInputPinWithPullUpresistor(Bank,PinNum);break;
				case 2:GPIO_setAsInputPinWithPullDownresistor(Bank,PinNum);break;
				default:
					ret = -1;
					*pErrNum = INVALID_PARAMETER;
					break;;
			}
		}
		break;

		case 1:
		{
			uint8_t state = (Config&0x08)>>3;
			GPIO_setAsOutputPin(Bank, PinNum);
			if(state == 1)
				GPIO_setOutputHighOnPin(Bank,PinNum);
			else
				GPIO_setOutputLowOnPin(Bank,PinNum);
		}
		break;

		case 2:
		{
			uint8_t IEdge = (Config&0x04)>>2;
			uint8_t IEnable = (Config&0x02)>>1;
			if(IEdge == 1){
				GPIO_setAsInputPinWithPullUpresistor(Bank,PinNum);
				GPIO_interruptEdgeSelect(Bank,PinNum,GPIO_HIGH_TO_LOW_TRANSITION);
			}else{
				GPIO_setAsInputPinWithPullDownresistor(Bank,PinNum);
				GPIO_interruptEdgeSelect(Bank,PinNum,GPIO_LOW_TO_HIGH_TRANSITION);
			}

			if(IEnable == 1)
				GPIO_disableInterrupt(Bank,PinNum);
			else
				GPIO_enableInterrupt(Bank,PinNum);
		}
		break;

		case 3:
		{
			uint8_t pfMode = Config&0x01;
			if(pfMode == 1)
				GPIO_setAsPeripheralModuleFunctionInputPin(Bank,PinNum);
			else
				GPIO_setAsPeripheralModuleFunctionOutputPin(Bank,PinNum);
		}
		break;

		default:
			ret = -1;
			*pErrNum = INVALID_PARAMETER;
			break;
	}

	return ret;
}

//******************************************************************************
//
//This is the PORT1_VECTOR interrupt vector service routine
//
//******************************************************************************
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=PORT1_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(PORT1_VECTOR)))
#endif
void Port_1(void)
{
    uint8_t val = 0xff;
    uint16_t hapticErr = 0;
    GPIO_setOutputHighOnPin(DRV_NRST);
    GPIO_setOutputHighOnPin(LED_NRST);
    StdI2C_P_RX_Single(DRV_ADDR, DRV_SET1, &val, &hapticErr);
    val &= 0x0c;
    if((val == 0x00) ||(val == 0x04)){
        GPIO_setAsOutputPin(DRV_TRG);
        if( GPIO_INPUT_PIN_LOW == GPIO_getInputPinValue(CC2650_TRIG_STATE)){
            GPIO_setOutputLowOnPin(DRV_TRG);
        }else{
            GPIO_setOutputHighOnPin(DRV_TRG);
        }
    }

    if( GPIO_INPUT_PIN_LOW == GPIO_getInputPinValue(CC2650_NRST_STATE)){
        GPIO_setOutputLowOnPin(DRV_NRST);
        GPIO_setOutputLowOnPin(LED_NRST);
    }else{
        GPIO_setOutputHighOnPin(DRV_NRST);
        GPIO_setOutputHighOnPin(LED_NRST);
    }

    if( GPIO_INPUT_PIN_LOW == GPIO_getInputPinValue(CC2650_LOAD_STATE)){
        GPIO_setOutputLowOnPin(DRV_Switch);
    }else{
        GPIO_setOutputHighOnPin(DRV_Switch);
    }

    //P1.2 IFG cleared
    GPIO_clearInterruptFlag(CC2650_INTERRUPT);
}
