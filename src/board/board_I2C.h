/*
 * board_I2C.h
 *
 *  Created on: Apr 3, 2014
 *      Author: a0220410
 */

#ifndef BOARD_I2C_H_
#define BOARD_I2C_H_

#include "StdPollI2C.h"
#include "StdI2C.h"
#include "RawI2C.h"

void initI2C();

#endif /* BOARD_I2C_H_ */
