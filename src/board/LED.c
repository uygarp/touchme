/*
 * LED.c
 *
 *  Created on: Mar 11, 2015
 *      Author: a0221097
 */
#include "stdint.h"
#include "board_gpio.h"
#include "gpio.h"
#include "HapticErrNo.h"
#include "board_config.h"
#include "LED.h"
#include "board_timer.h"
#include "string.h"

void Board_FlashLED(unsigned int numberOfBlinks)
{
	unsigned int i = 0;
	//GPIO_setOutputHighOnPin(GPIO_PORT_P4,GPIO_PIN0 );
	//GPIO_setOutputHighOnPin(LED_PORT_D,LED_PIN_D1 );
	//GPIO_setOutputHighOnPin(LED_PORT_D,LED_DALL);
	//GPIO_setOutputHighOnPin(LED_PORT_M,LED_PIN_MODEALL);
	//timerdelay(LEDBLINKDELAY);
	for(i = 0; i < numberOfBlinks; i++)
	{
		LED_Toggle(LED_PORT_D,LED_DALL);
		LED_Toggle(LED_PORT_M,LED_MODEALL);
		timerdelay(2000);                           //100us * 2000
		LED_Toggle(LED_PORT_D,LED_DALL);
		LED_Toggle(LED_PORT_M,LED_MODEALL);
		timerdelay(2000);
	}

}

void LED_On(uint8_t LEDPort, uint8_t LED_ID)
{
	GPIO_setOutputHighOnPin(LEDPort ,LED_ID);
}

void LED_Off(uint8_t LEDPort, uint8_t LED_ID)
{
	GPIO_setOutputLowOnPin(LEDPort ,LED_ID);
}

void LED_Toggle(uint8_t LEDPort, uint8_t LED_ID)
{
	GPIO_toggleOutputOnPin(LEDPort, LED_ID);
}
void LED_Blink(uint8_t LEDPort, uint8_t LED_ID)
{
	GPIO_toggleOutputOnPin(LEDPort, LED_ID);
	timerdelay(100);
	GPIO_toggleOutputOnPin(LEDPort, LED_ID);
	timerdelay(100);
}

void LED_MODE(uint8_t mode)
{
	if((mode & 0x08) != 0)
		LED_On(LED_PORT_M, LED_M0);
	else
		LED_Off(LED_PORT_M, LED_M0);

	if((mode & 0x04) != 0)
		LED_On(LED_PORT_M, LED_M1);
	else
		LED_Off(LED_PORT_M, LED_M1);

	if((mode & 0x02) != 0)
		LED_On(LED_PORT_M, LED_M2);
	else
		LED_Off(LED_PORT_M, LED_M2);

	if((mode & 0x01) != 0)
		LED_On(LED_PORT_M, LED_M3);
	else
		LED_Off(LED_PORT_M, LED_M3);
}
