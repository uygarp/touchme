/*
 * board_timer.c
 *
 *  Created on: Apr 1, 2014
 *      Author: a0220410
 */
#include "driverlib.h"
#include "timer_a.h"
#include "HapticErrNo.h"
#include "board_timer.h"
#include "board_config.h"

static uint8_t g_isPWMOn = 0;
static uint16_t g_PWMPeriod = 0;

void board_timer_init(){

}

static void DualPWM_Start(uint16_t timerPeriod, uint16_t dutyCycle)
{
	GPIO_setAsPeripheralModuleFunctionOutputPin(GPIO_PORT_P1,GPIO_PIN2 + GPIO_PIN3);

	g_PWMPeriod = timerPeriod;
	TIMER_A_configureUpDownMode(PWM_TIMER,
                            TIMER_A_CLOCKSOURCE_SMCLK,
                            PWM_CLOCKSOURCE_DIVIDER,
                            timerPeriod,
                            TIMER_A_TAIE_INTERRUPT_DISABLE,
                            TIMER_A_CCIE_CCR0_INTERRUPT_DISABLE,
                            TIMER_A_DO_CLEAR
                            );

    TIMER_A_startCounter(PWM_TIMER,TIMER_A_UPDOWN_MODE);

    //Initialize compare mode to generate PWM1
    TIMER_A_initCompare(PWM_TIMER,
    					PWM_CCR_1P2,
                        TIMER_A_CAPTURECOMPARE_INTERRUPT_DISABLE,
                        TIMER_A_OUTPUTMODE_TOGGLE_RESET,
                        dutyCycle
                        );

    //Initialize compare mode to generate PWM2
    TIMER_A_initCompare(PWM_TIMER,
    					PWM_CCR_1P3,
    					TIMER_A_CAPTURECOMPARE_INTERRUPT_DISABLE,
    					TIMER_A_OUTPUTMODE_TOGGLE_SET,
    					timerPeriod - dutyCycle
                        );

}

static void PWM_SetDutyCycle(uint16_t period, uint16_t dutyCycle)
{
	TIMER_A_setCompareValue(PWM_TIMER, PWM_CCR_1P2, dutyCycle);
	TIMER_A_setCompareValue(PWM_TIMER, PWM_CCR_1P3, period - dutyCycle);
}

static void PWM_SetPeriod(uint16_t period)
{
	if(g_PWMPeriod != period){
		TIMER_A_setCounterValue(PWM_TIMER, period);
		g_PWMPeriod = period;
	}
}

static void PWM_Stop()
{
	g_PWMPeriod = 0;
	TIMER_A_stop(PWM_TIMER);
	GPIO_setAsOutputPin(GPIO_PORT_P1,GPIO_PIN2 + GPIO_PIN3);
	GPIO_setOutputLowOnPin(GPIO_PORT_P1,GPIO_PIN2 + GPIO_PIN3);
}

int DualPWMConfig(uint16_t freq, uint16_t duty, uint8_t sw, uint16_t *pErrNo)
{
	int ret = 0;
	uint16_t period = PWM_NUMOFCOUNTER/freq;
	uint16_t dutycycle = ((uint32_t)period*duty)/100;

	if((dutycycle < period)&&(sw==1)&&(g_isPWMOn == 0)){
		//turn on PWM
		DualPWM_Start(period, dutycycle);
		g_isPWMOn = 1;
	}else if((sw==0)&&(g_isPWMOn == 1)){
		//switch off PWM
		PWM_Stop();
		g_isPWMOn = 0;
	}else if((sw==1)&&(g_isPWMOn == 1)&&(dutycycle < period)){
		//update duty cycle
		PWM_SetPeriod(period);
		PWM_SetDutyCycle(period, dutycycle);
	}else if((sw==0)&&(g_isPWMOn == 0)){
		//pass
	}else{
		ret = -1;
	}

	if(ret == 0) *pErrNo = NO_ERROR;
	else *pErrNo = INVALID_PARAMETER;

	return ret;
}



//100us timer delay
void timerdelay(unsigned int tdelay)
{
	TIMER_A_configureUpMode(PERIODICAL_TIMER,
								TIMER_A_CLOCKSOURCE_ACLK,
								TIMER_A_CLOCKSOURCE_DIVIDER_1,
								tdelay,
								TIMER_A_TAIE_INTERRUPT_DISABLE,
								TIMER_A_CCIE_CCR0_INTERRUPT_ENABLE,
								TIMER_A_DO_CLEAR
								);

		//Initiaze compare mode
		TIMER_A_clearCaptureCompareInterruptFlag(PERIODICAL_TIMER,
												 TIMER_A_CAPTURECOMPARE_REGISTER_0
												 );

	//	TIMER_A_initCompare(PERIODICAL_TIMER,
	//						TIMER_A_CAPTURECOMPARE_REGISTER_0,
	//						TIMER_A_CAPTURECOMPARE_INTERRUPT_ENABLE,
	//						TIMER_A_OUTPUTMODE_OUTBITVALUE,
	//						tdelay
	//						)

		//Start Counter
		TIMER_A_startCounter(PERIODICAL_TIMER, TIMER_A_CONTINUOUS_MODE);


	    __bis_SR_register(LPM0_bits+GIE);		//Go into Low Power Mode (CPU/MCLK off)
	    __no_operation();
}

//100us sleep
void sleep(unsigned int time)
{
	TIMER_A_configureUpMode(PERIODICAL_TIMER,
								TIMER_A_CLOCKSOURCE_ACLK,
								TIMER_A_CLOCKSOURCE_DIVIDER_1,
								time,
								TIMER_A_TAIE_INTERRUPT_DISABLE,
								TIMER_A_CCIE_CCR0_INTERRUPT_DISABLE,
								TIMER_A_DO_CLEAR
								);

	TIMER_A_clearCaptureCompareInterruptFlag(PERIODICAL_TIMER,
											 TIMER_A_CAPTURECOMPARE_REGISTER_0
											 );

	//	TIMER_A_initCompare(PERIODICAL_TIMER,
	//						TIMER_A_CAPTURECOMPARE_REGISTER_0,
	//						TIMER_A_CAPTURECOMPARE_INTERRUPT_ENABLE,
	//						TIMER_A_OUTPUTMODE_OUTBITVALUE,
	//						tdelay
	//						)

	//Start Counter
	TIMER_A_startCounter(PERIODICAL_TIMER, TIMER_A_CONTINUOUS_MODE);


	__bis_SR_register(LPM0_bits+GIE);		//Go into Low Power Mode (CPU/MCLK off)
	__no_operation();
}

