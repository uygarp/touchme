/*
 * StdSpi.h
 *
 *  Created on: Apr 3, 2014
 *      Author: a0220410
 */

#ifndef STDSPI_H_
#define STDSPI_H_

extern uint8_t pBuffStdSPIRx[];
extern uint8_t pBuffStdSPITx[];
extern volatile uint8_t nSPIRTLen;
extern volatile uint8_t nSPIRTCount;

int StdSpi_single_Write(uint8_t reg, uint8_t val);
int StdSpi_bulk_Write(uint8_t reg, uint8_t count, uint8_t *pData);
//int StdSpi_bulk_Read(uint8_t reg, uint8_t count, uint8_t *pData);
int StdSpi_single_Read(uint8_t reg, uint8_t *pVal);
#endif /* STDSPI_H_ */
