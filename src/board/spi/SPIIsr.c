/*
 * SPIIsr.c
 *
 *  Created on: Apr 3, 2014
 *      Author: a0220410
 */
#include "usci_a_spi.h"
#include "StdSpi.h"
//******************************************************************************
//
//This is the USCI_A1 interrupt vector service routine.
//
//******************************************************************************
#if 0
static volatile int irq = 0;
#if defined(__TI_COMPILER_VERSION__) || defined(__IAR_SYSTEMS_ICC__)
#pragma vector=USCI_A1_VECTOR
__interrupt
#elif defined(__GNUC__)
__attribute__((interrupt(USCI_A1_VECTOR)))
#endif
void USCI_A1_ISR(void)
{
		irq = __even_in_range(UCA1IV, 4);
        switch (irq) {
			//Vector 2 - RXIFG
			case 2:
				if(nSPIRTCount < nSPIRTLen){
					pBuffStdSPIRx[nSPIRTCount++] = USCI_A_SPI_receiveData(USCI_A1_BASE);
				}else{
					USCI_A_SPI_disableInterrupt(USCI_A1_BASE, USCI_A_SPI_RECEIVE_INTERRUPT);
				}
			 break;

			 //Vector 4 - TXIFG
			case 4:
				if(nSPIRTCount < nSPIRTLen){
					USCI_A_SPI_transmitData(USCI_A1_BASE, pBuffStdSPITx[nSPIRTCount++]);
				}else{
					USCI_A_SPI_disableInterrupt(USCI_A1_BASE, USCI_A_SPI_TRANSMIT_INTERRUPT);
				}
				break;

        default: break;
        }
}
#endif
