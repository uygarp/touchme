/*
 * StdSpi.c
 *
 *  Created on: Apr 3, 2014
 *      Author: a0220410
 */
#include "stdint.h"
#include "StdSpi.h"
#include "usci_a_spi.h"
#include "board_config.h"

uint8_t pBuffStdSPIRx[SPI_RTX_BUF_LEN];
uint8_t pBuffStdSPITx[SPI_RTX_BUF_LEN];
volatile uint8_t nSPIRTLen = 0;
volatile uint8_t nSPIRTCount = 0;

int StdSpi_bulk_Write(uint8_t reg, uint8_t count, uint8_t *pData)
{
	int i=0;

	pBuffStdSPITx[0] = reg<<1;

	for(i = 0; i < count; i++){
		pBuffStdSPITx[i+1] = pData[i];
	}

	USCI_A_SPI_enable(USCI_A1_BASE);

	for(i = 0; i < (count+1); i++){

	    while (!USCI_A_SPI_getInterruptStatus(USCI_A1_BASE,
	                                          USCI_A_SPI_TRANSMIT_INTERRUPT)) ;

	    USCI_A_SPI_transmitData(USCI_A1_BASE, pBuffStdSPITx[i]);
	}

	while (!USCI_A_SPI_getInterruptStatus(USCI_A1_BASE,
		                                          USCI_A_SPI_TRANSMIT_INTERRUPT)) ;

	USCI_A_SPI_disable(USCI_A1_BASE);
	return count;
}

int StdSpi_single_Write(uint8_t reg, uint8_t val)
{
#if 0
	// the interrupt services seems slower

	pBuffStdSPITx[0] = reg<<1;

	pBuffStdSPITx[1] = val;

	nSPIRTLen = 2;
	nSPIRTCount = 0;

    USCI_A_SPI_enableInterrupt(USCI_A1_BASE,
    		USCI_A_SPI_TRANSMIT_INTERRUPT);

    while(USCI_A_SPI_isBusy(USCI_A1_BASE));

    if(nSPIRTCount == nSPIRTLen){
    	return nSPIRTCount;
    }else{
    	return -1;
    }

#else
    return StdSpi_bulk_Write(reg, 1, &val);
#endif

}

//not sure how SPI device will use this, will update this function later
static int StdSpi_bulk_Read(uint8_t reg, uint8_t count, uint8_t *pData)
{
	int i;
	int loop = count +1;
	uint8_t regbyte = (reg << 1)|0x01;

	USCI_A_SPI_enable(USCI_A1_BASE);

	for(i=0; i < loop; i++){

	    //USCI_A0 TX buffer ready?
	    while (!USCI_A_SPI_getInterruptStatus(USCI_A1_BASE,
	                                          USCI_A_SPI_TRANSMIT_INTERRUPT)) ;

	    USCI_A_SPI_transmitData(USCI_A1_BASE, regbyte);

	    if(i != 0){
		    //USCI_A0 TX buffer ready?
		    while (!USCI_A_SPI_getInterruptStatus(USCI_A1_BASE,
		    		USCI_A_SPI_RECEIVE_INTERRUPT)) ;
		    pBuffStdSPIRx[i-1] = USCI_A_SPI_receiveData(USCI_A1_BASE);
	    }

	}

    USCI_A_SPI_disable(USCI_A1_BASE);
	memcpy(pData, pBuffStdSPIRx, count);

    return count;
}

int StdSpi_single_Read(uint8_t reg, uint8_t *pVal)
{
	return StdSpi_bulk_Read(reg, 1, pVal);
}


