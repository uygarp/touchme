/*
 * gpio.h
 *
 *  Created on: Apr 1, 2014
 *      Author: a0220410
 */

#ifndef BOARD_GPIO_H_
#define BOARD_GPIO_H_

#include "stdint.h"
#include "gpio.h"

void board_GPIO_config();
int GPIO_Config(uint8_t Config, uint8_t Bank, uint16_t PinNum, uint16_t *pErrNum);

#endif /* BOARD_GPIO_H_ */
