/*
 * HapticCmdDispatcher.c
 *
 *  Created on: Mar 21, 2014
 *      Author: a0220410
 */

#include "HapticCmdDispatcher.h"
#include "HapticErrNo.h"
#include "HapticsTarget.h"
#include "string.h"
#include "board.h"

static uint16_t g_ErrorNo = 0;

uint8_t HapticCmdBuf[HAPTIC_CMD_BUF_SIZE];

uint16_t HapticCmdDispatch(uint16_t count)
{
    count = 6;
	int rs = 0;
	uint16_t HapticCmdId = (uint16_t)(HapticCmdBuf[HAPTIC_REP_ID_OFFSET_H]<<8)|HapticCmdBuf[HAPTIC_REP_ID_OFFSET_L];
	HapticCmdId = HAPTICS_REPORT_SINGLE_READ_OUT;
	g_ErrorNo = NO_ERROR;

	switch(HapticCmdId){
		case HAPTICS_REPORT_SINGLE_WRITE_OUT:
		{
			uint8_t SlaveAddr = HapticCmdBuf[HAPTIC_REP_ADDR_OFFSET];
			uint8_t RegAddr = HapticCmdBuf[HAPTIC_REP_REGADDR_OFFSET];
			uint8_t Val = HapticCmdBuf[HAPTIC_REP_REGVAL_OFFSET];

			rs = StdI2C_P_TX_Single(SlaveAddr, RegAddr, Val, &g_ErrorNo);

			HapticCmdBuf[HAPTIC_REP_ID_OFFSET_L] = (uint8_t)HAPTICS_REPORT_SINGLE_WRITE_IN;
			HapticCmdBuf[HAPTIC_REP_ID_OFFSET_H] = (uint8_t)(HAPTICS_REPORT_SINGLE_WRITE_IN>>8);
			HapticCmdBuf[count -3] = (uint8_t)g_ErrorNo;
			HapticCmdBuf[count -2] = (uint8_t)(g_ErrorNo>>8);
			if((rs == 1)&&(g_ErrorNo==NO_ERROR)){
				HapticCmdBuf[count -1] = 1;
			}else{
				HapticCmdBuf[count -1] = 0;
			}
		}
		break;

		case HAPTICS_REPORT_SINGLE_READ_OUT:
		{
			uint8_t SlaveAddr = HapticCmdBuf[HAPTIC_REP_ADDR_OFFSET];
			uint8_t RegAddr = HapticCmdBuf[HAPTIC_REP_REGADDR_OFFSET];
			uint8_t RegVal = 0;

			//rs = StdI2C_P_RX_Single(SlaveAddr, RegAddr, &RegVal, &g_ErrorNo);
			rs = 1;
			RegVal = 'x';
			g_ErrorNo = NO_ERROR;

			HapticCmdBuf[HAPTIC_REP_ID_OFFSET_L] = (uint8_t)HAPTICS_REPORT_SINGLE_READ_IN;
			HapticCmdBuf[HAPTIC_REP_ID_OFFSET_H] = (uint8_t)(HAPTICS_REPORT_SINGLE_READ_IN>>8);
			HapticCmdBuf[count -3] = (uint8_t)g_ErrorNo;
			HapticCmdBuf[count -2] = (uint8_t)(g_ErrorNo>>8);
			if((rs == 1)&&(g_ErrorNo==NO_ERROR)){
				HapticCmdBuf[HAPTIC_REP_DATALEN_OFFSET] += 1;
				HapticCmdBuf[count -4] = RegVal;
				HapticCmdBuf[count -1] = 1;
			}else{
				HapticCmdBuf[count -1] = 0;
			}
		}
		break;

		case HAPTICS_REPORT_MULTI_WRITE_OUT:
		{
			uint8_t SlaveAddr = HapticCmdBuf[HAPTIC_REP_ADDR_OFFSET];
			uint8_t RegAddr = HapticCmdBuf[HAPTIC_REP_REGADDR_OFFSET];
			uint8_t Count = HapticCmdBuf[HAPTIC_REP_DATALEN_OFFSET] - 4;

			rs = StdI2C_P_TX_Multiple(SlaveAddr, RegAddr, Count, &HapticCmdBuf[HAPTIC_REP_REGVAL_OFFSET], &g_ErrorNo);

			HapticCmdBuf[HAPTIC_REP_ID_OFFSET_L] = (uint8_t)HAPTICS_REPORT_MULTI_WRITE_IN;
			HapticCmdBuf[HAPTIC_REP_ID_OFFSET_H] = (uint8_t)(HAPTICS_REPORT_MULTI_WRITE_IN>>8);
			HapticCmdBuf[count -3] = (uint8_t)g_ErrorNo;
			HapticCmdBuf[count -2] = (uint8_t)(g_ErrorNo>>8);
			if((rs == Count)&&(g_ErrorNo==NO_ERROR)){
				HapticCmdBuf[count -1] = Count;
			}else{
				HapticCmdBuf[count -1] = 0;
			}
		}
		break;

		case HAPTICS_REPORT_MULTI_READ_OUT:
		{
			uint8_t SlaveAddr = HapticCmdBuf[HAPTIC_REP_ADDR_OFFSET];
			uint8_t RegAddr = HapticCmdBuf[HAPTIC_REP_REGADDR_OFFSET];
			uint8_t Count = HapticCmdBuf[HAPTIC_REP_REGCNT_OFFSET];

			if(Count == 1){
				rs = StdI2C_P_RX_Single(SlaveAddr, RegAddr, &HapticCmdBuf[HAPTIC_REP_REGCNT_OFFSET+1], &g_ErrorNo);
			}else{
				rs = StdI2C_P_RX_Multiple(SlaveAddr, RegAddr, Count, &HapticCmdBuf[HAPTIC_REP_REGCNT_OFFSET+1], &g_ErrorNo);
			}

			HapticCmdBuf[HAPTIC_REP_ID_OFFSET_L] = (uint8_t)HAPTICS_REPORT_MULTI_READ_IN;
			HapticCmdBuf[HAPTIC_REP_ID_OFFSET_H] = (uint8_t)(HAPTICS_REPORT_MULTI_READ_IN>>8);
			HapticCmdBuf[count -3] = (uint8_t)g_ErrorNo;
			HapticCmdBuf[count -2] = (uint8_t)(g_ErrorNo>>8);
			if((rs == Count)&&(g_ErrorNo==NO_ERROR)){
				HapticCmdBuf[HAPTIC_REP_DATALEN_OFFSET] += Count;
				HapticCmdBuf[count -1] = Count;
			}else{
				HapticCmdBuf[count -1] = 0;
			}
		}
		break;

		case HAPTICS_REPORT_SINGLE_SETBIT_OUT:
		{
			uint8_t SlaveAddr = HapticCmdBuf[HAPTIC_REP_ADDR_OFFSET];
			uint8_t RegAddr = HapticCmdBuf[HAPTIC_REP_REGADDR_OFFSET];
			uint8_t RegMsk = HapticCmdBuf[HAPTIC_REP_REGMSK_OFFSET];
			uint8_t RegBit = HapticCmdBuf[HAPTIC_REP_REGBIT_OFFSET];
			uint8_t RegVal = 0;

			rs = StdI2C_P_RX_Single(SlaveAddr, RegAddr, &RegVal, &g_ErrorNo);
			if((rs == 1)&&(g_ErrorNo==NO_ERROR)){
				if((RegVal & RegMsk) != (RegBit & RegMsk)){
					RegVal = (RegVal&~RegMsk)|(RegBit&RegMsk);
					rs = StdI2C_P_TX_Single(SlaveAddr, RegAddr, RegVal, &g_ErrorNo);
				}
			}

			HapticCmdBuf[HAPTIC_REP_ID_OFFSET_L] = (uint8_t)HAPTICS_REPORT_SINGLE_SETBIT_IN;
			HapticCmdBuf[HAPTIC_REP_ID_OFFSET_H] = (uint8_t)(HAPTICS_REPORT_SINGLE_SETBIT_IN>>8);
			HapticCmdBuf[count -3] = (uint8_t)g_ErrorNo;
			HapticCmdBuf[count -2] = (uint8_t)(g_ErrorNo>>8);
			if((rs == 1)&&(g_ErrorNo==NO_ERROR)){
				HapticCmdBuf[count -1] = 1;
			}else{
				HapticCmdBuf[count -1] = 0;
			}
		}
		break;

		case HAPTICS_REPORT_MULTI_SETBIT_OUT:
		{
			uint8_t SlaveAddr = HapticCmdBuf[HAPTIC_REP_ADDR_OFFSET];
			uint8_t RegNum = (HapticCmdBuf[HAPTIC_REP_DATALEN_OFFSET] - 3)/3;
			uint8_t RegAddr = 0;
			uint8_t RegMsk = 0;
			uint8_t RegBit = 0;
			uint8_t RegVal = 0;
			int i;

			for( i=0; i<RegNum; i++){
				RegAddr = HapticCmdBuf[HAPTIC_REP_REGPAGE_OFFSET + 1 + 3*i];
				RegMsk = HapticCmdBuf[HAPTIC_REP_REGPAGE_OFFSET + 2 + 3*i];
				RegBit = HapticCmdBuf[HAPTIC_REP_REGPAGE_OFFSET + 3 + 3*i];
				rs = StdI2C_P_RX_Single(SlaveAddr, RegAddr, &RegVal, &g_ErrorNo);
				if((rs == 1)&&(g_ErrorNo==NO_ERROR)){
					if((RegVal & RegMsk) != (RegBit & RegMsk)){
						RegVal = (RegVal&~RegMsk)|(RegBit&RegMsk);
						rs = StdI2C_P_TX_Single(SlaveAddr, RegAddr, RegVal, &g_ErrorNo);
						if((rs != 1)||(g_ErrorNo!=NO_ERROR)){
							break;
						}
					}
				}else{
					break;
				}
			}

			HapticCmdBuf[HAPTIC_REP_ID_OFFSET_L] = (uint8_t)HAPTICS_REPORT_MULTI_SETBIT_IN;
			HapticCmdBuf[HAPTIC_REP_ID_OFFSET_H] = (uint8_t)(HAPTICS_REPORT_MULTI_SETBIT_IN>>8);
			HapticCmdBuf[count -3] = (uint8_t)g_ErrorNo;
			HapticCmdBuf[count -2] = (uint8_t)(g_ErrorNo>>8);
			if((rs == 1)&&(g_ErrorNo==NO_ERROR)){
				HapticCmdBuf[count -1] = RegNum;
			}else{
				HapticCmdBuf[count -1] = 0;
			}
		}
		break;

        case HAPTICS_REPORT_TARGET_DETECT_OUT:
		{
			uint8_t StrLen = 0;

			StrLen = strlen(TARGET_DRV2625EVM);
			memcpy(&HapticCmdBuf[HAPTIC_REP_PORT_OFFSET+1], TARGET_DRV2625EVM, StrLen);

			HapticCmdBuf[HAPTIC_REP_ID_OFFSET_L] = (uint8_t)HAPTICS_REPORT_TARGET_DETECT_IN;
			HapticCmdBuf[HAPTIC_REP_ID_OFFSET_H] = (uint8_t)(HAPTICS_REPORT_TARGET_DETECT_IN>>8);
			HapticCmdBuf[HAPTIC_REP_DATALEN_OFFSET] += StrLen;
			HapticCmdBuf[count -3] = 0x00;
			HapticCmdBuf[count -2] = 0x00;
			HapticCmdBuf[count -1] = StrLen;
		}
		break;

		case HAPTICS_REPORT_GPIO_CONFIG_OUT:
		{
			uint8_t Config = HapticCmdBuf[HAPTIC_REP_GPIO_CONFIG_OFFSET];
			uint8_t BankCount = HapticCmdBuf[HAPTIC_REP_GPIO_BANKCOUNT_OFFSET];
			uint8_t Bank = 0, pin_LSB = 0,pin_MSB = 0, ic = 0;
			uint16_t pin = 0;

			for(ic=0;ic<BankCount;ic++)
			{
				Bank = HapticCmdBuf[HAPTIC_REP_GPIO_BANK_OFFSET + ic*3];
				pin_LSB = HapticCmdBuf[HAPTIC_REP_GPIO_PINNUM_OFFSET + ic*3];
				pin_MSB = HapticCmdBuf[HAPTIC_REP_GPIO_PINNUM_OFFSET + 1 + ic*3];
				pin = ((uint16_t)(pin_MSB<<8)|pin_LSB);
				rs = GPIO_Config(Config, Bank, pin, &g_ErrorNo);
				if((rs < 0)||(g_ErrorNo!=NO_ERROR)){
					break;
				}
			}

			HapticCmdBuf[HAPTIC_REP_ID_OFFSET_L] = (uint8_t)HAPTICS_REPORT_GPIO_CONFIG_IN;
			HapticCmdBuf[HAPTIC_REP_ID_OFFSET_H] = (uint8_t)(HAPTICS_REPORT_GPIO_CONFIG_IN>>8);
			HapticCmdBuf[count -3] = (uint8_t)g_ErrorNo;
			HapticCmdBuf[count -2] = (uint8_t)(g_ErrorNo>>8);
			if((rs>=0)&&(g_ErrorNo==NO_ERROR))
				HapticCmdBuf[count -1] = BankCount;
			else
				HapticCmdBuf[count -1] = 0;
		}
		break;
		
		case HAPTICS_REPORT_ECHO_OUT:
		{
			HapticCmdBuf[HAPTIC_REP_ID_OFFSET_L] = (uint8_t)HAPTICS_REPORT_ECHO_IN;
			HapticCmdBuf[HAPTIC_REP_ID_OFFSET_H] = (uint8_t)(HAPTICS_REPORT_ECHO_IN>>8);
			HapticCmdBuf[count -3] = 'k';
			HapticCmdBuf[count -2] = 0;
			HapticCmdBuf[count -1] = HapticCmdBuf[HAPTIC_REP_DATALEN_OFFSET];
		}
		break;

		case HAPTICS_REPORT_USB_QUIT_OUT:
		{
            HapticCmdBuf[HAPTIC_REP_ID_OFFSET_L] = (uint8_t)HAPTICS_REPORT_USB_QUIT_IN;
            HapticCmdBuf[HAPTIC_REP_ID_OFFSET_H] = (uint8_t)(HAPTICS_REPORT_USB_QUIT_IN>>8);
            HapticCmdBuf[count -1] = 1;
		}
		break;

		default:
		{
			g_ErrorNo = UNKNOWN_REPORT;
			HapticCmdBuf[count -3] = (uint8_t)g_ErrorNo;
			HapticCmdBuf[count -2] = (uint8_t)(g_ErrorNo>>8);
			HapticCmdBuf[count -1] = 0;
		}
		break;
	}

	return HapticCmdId;
}

