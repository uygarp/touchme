/*
 * INA231.c
 *
 *  Created on: May 14, 2015
 *      Author: a0220410

*/
#include "INA231_EEPR.h"
#include "board.h"
#include "HapticErrNo.h"
#include <stdint.h>

int INA231_RegWrite(uint8_t reg, uint16_t val){
	int ret = 0;
	uint16_t error = 0;
	uint8_t data[2] = {(val&0xff00)>>8, val&0x00ff};

	ret = StdI2C_P_TX_Multiple(INA231_I2CADDR, reg, 2, data, &error);

	if((ret == 2)&&(error == NO_ERROR))
		return 1;
	else if(ret < 0)
		return ret;
	else if(error > NO_ERROR)
		return -error;
	else
		return -1;
}

int INA231_RegRead(uint8_t reg){
	int ret = 0;
	uint16_t error = 0;
	uint8_t data[2];

	ret = StdI2C_P_RX_Multiple(INA231_I2CADDR, reg, 2, data, &error);

	if((ret == 2)&&(error == NO_ERROR))
		return (((uint16_t)data[0]<<8)||data[1]);
	else if(ret < 0)
		return ret;
	else if(error > NO_ERROR)
		return -error;
	else
		return -1;
}



