
#include <string.h>
#include <stdio.h>
#include "msp430f5510.h"
#include "driverlib.h"
#include "USB_config/descriptors.h"
#include "USB_API/USB_Common/device.h"
#include "USB_API/USB_Common/types.h"               // Basic Type declarations
#include "USB_API/USB_Common/usb.h"                 // USB-specific functions
#include "USB_API/USB_HID_API/UsbHid.h"
#include "USB_app/usbConstructs.h"
/*
 * NOTE: Modify hal.h to select a specific evaluation board and customize for
 * your own board.
 */
#include "hal.h"
#include "board.h"
#include "board_uart.h"
#include "HapticCmdDispatcher.h"
#include "LED.h"
#include "Capacitive_Touch/CTS_Layer.h"
#include "Capacitive_Touch/structure.h"
#include "DRV2625.h"
#include "Board_Functions.h"
#include "Haptics.h"
#include "HapticErrNo.h"
#include "braille.h"
#include "Actuator_Waveforms.h"

// Global flags set by events
volatile BYTE bHIDDataReceived_event = FALSE;  // Flag set by event handler to
volatile uint8_t Button_Released = 0;
volatile uint8_t modecounter = 0;
int x=0;
#define INNER_WHEEL_DATA
#define INNER_WHEEL_POSITION
#define NUMBER_OF_TPG_CHANNELS	4
uint16_t innerPos;
uint16_t innerPosOld = ILLEGAL_SLIDER_WHEEL_POSITION;
volatile uint8_t i;
uint16_t cinnerPos;
uint16_t lbyte, hbyte;
uint16_t dInner[4];

uint16_t holdcounter =0;
uint16_t freecounter =0;

uint16_t hapticErr;
bool m4ermlra = false;
bool wheel_release = false;
uint8_t usbmode = 0;
bool read_enabled = true;
bool button_released = true;
static void receive_USB_data(uint8_t * received_byte);
static void send_USB_data(uint8_t received_byte);
bool buttons_fetched = false;
bool first_line_fetched = false;
uint8_t line1[3] = {};
uint8_t line2[3] = {};

struct Element* buttonPtr;

static void perform_braille_code(uint8_t byte);
static void perform_line_code(uint8_t line);
static void between_braille_lines();
static void change_mode();
static void reset_buttons();
static void update_leds();
static void send_mode_loop();

static void send_mode_loop()
{
    static uint8_t counter = 0;
    if(counter++ > 50)
    {
        counter = 0;
        send_USB_data(read_enabled ? 'R' : 'W');
    }
}

static void receive_USB_data(uint8_t * received_byte)
{
    *received_byte = NULL;
    switch (USB_connectionState())
    {
        case ST_ENUM_ACTIVE:
            __disable_interrupt();
            if (!USBHID_bytesInUSBBuffer(HID0_INTFNUM))
            {
                _NOP();
            }
            __enable_interrupt();
            if (bHIDDataReceived_event)
            {
                bHIDDataReceived_event = FALSE;
                hidReceiveDataInBuffer(HapticCmdBuf,
                        HAPTIC_CMD_BUF_SIZE, HID0_INTFNUM);
                if(HapticCmdBuf[0] != 0 && HapticCmdBuf[0] != 10)
                {
                    *received_byte = HapticCmdBuf[0];
                }
            }
            break;
            // These cases are executed while your device is disconnected from
            // the host (meaning, not enumerated); enumerated but suspended
            // by the host, or connected to a powered hub without a USB host
            // present.
        case ST_PHYS_DISCONNECTED:
        case ST_ENUM_SUSPENDED:
        case ST_PHYS_CONNECTED_NOENUM_SUSP:

            //  __bis_SR_register(LPM3_bits + GIE);
            _NOP();
            //Run_Button_USB_Wake();
            //USBUse = false;
            break;
            // The default is executed for the momentary state
            // ST_ENUM_IN_PROGRESS.  Usually, this state only last a few
            // seconds.  Be sure not to enter LPM3 in this state; USB
            // communication is taking place here, and therefore the mode must
            // be LPM0 or active-CPU.
        case ST_ENUM_IN_PROGRESS:
        default:;
    }
}

static void send_USB_data(uint8_t received_byte)
{
    switch (USB_connectionState())
    {
        case ST_ENUM_ACTIVE:
            __disable_interrupt();
            if (!USBHID_bytesInUSBBuffer(HID0_INTFNUM))
            {
                _NOP();
            }
            __enable_interrupt();
            HapticCmdBuf[0] = received_byte;
            hidSendDataInBackground(HapticCmdBuf, 1, HID0_INTFNUM, 1);
            break;
            // These cases are executed while your device is disconnected from
            // the host (meaning, not enumerated); enumerated but suspended
            // by the host, or connected to a powered hub without a USB host
            // present.
        case ST_PHYS_DISCONNECTED:
        case ST_ENUM_SUSPENDED:
        case ST_PHYS_CONNECTED_NOENUM_SUSP:

            //  __bis_SR_register(LPM3_bits + GIE);
            _NOP();
            //Run_Button_USB_Wake();
            //USBUse = false;
            break;
            // The default is executed for the momentary state
            // ST_ENUM_IN_PROGRESS.  Usually, this state only last a few
            // seconds.  Be sure not to enter LPM3 in this state; USB
            // communication is taking place here, and therefore the mode must
            // be LPM0 or active-CPU.
        case ST_ENUM_IN_PROGRESS:
        default:;
    }
}

static void reset_buttons()
{
    holdcounter = 0;
    reset_wheel_buttons();
    reset_capt_buttons();
}

static void update_leds()
{
    if(read_enabled)
    {
        LED_Off(LED_PORT_PM, LED_PORT_PM_ALL);
        LED_On(LED_PORT_M, LED_DALL);
    }
    else
    {
        LED_On(LED_PORT_PM, LED_PORT_PM_ALL);
        LED_Off(LED_PORT_M, LED_DALL);
    }
}

static void change_mode()
{
    read_enabled = !read_enabled;
    first_line_fetched = false;
    //send_USB_data(read_enabled ? 'R' : 'W');
    update_leds();
}

static void wait(uint8_t wait_ms)
{
    int k = 0, i = 0;
    for(k = 0; k < 4 * wait_ms; k++)
    {
        for (i = 0; i < (10000); i++){
            _NOP();
        }
    }
}

static void perform_line_code(uint8_t line)
{
    switch(line)
    {
        case 0:
            drv_run(Actuator_ERM, TripleClick_100);
            wait(5);
            drv_run(Actuator_LRA, TripleClick_100);
            break;
        case 1:
            drv_run(Actuator_ERM, SharpTick1_100);
            wait(5);
            drv_run(Actuator_LRA, SharpTick1_100);
            break;
        case 2:
            drv_run(Actuator_ERM, PulsingSharp1_100);
            wait(5);
            drv_run(Actuator_LRA, PulsingSharp1_100);
            break;
        case 3:
            drv_run(Actuator_ERM, SoftBump_100);
            wait(5);
            drv_run(Actuator_LRA, TransitionRampDownShortSmooth1_100to0);
            break;
        case 4:
            drv_run(Actuator_ERM, SoftBump_100);
            wait(5);
            drv_run(Actuator_LRA, SoftBump_100);
            break;
        case 5:
            drv_run(Actuator_ERM, BuzzAlert750ms);
            wait(5);
            drv_run(Actuator_LRA, BuzzAlert750ms);
            break;
        case 6:
            drv_run(Actuator_ERM, DoubleClick_100);
            wait(5);
            drv_run(Actuator_LRA, DoubleClick_100);
            break;
        case 7:
            drv_run(Actuator_ERM, Buzz1_100);
            wait(5);
            drv_run(Actuator_LRA, Buzz1_100);
            break;
    }
}

static void between_braille_lines()
{
    wait(100);
}

static void perform_braille_code(uint8_t byte)
{
    uint8_t line1, line2;
    uint8_t ret = char2braille(byte, &line1, &line2);

    if(ret)
    {
        perform_line_code(line1);
        between_braille_lines();
        perform_line_code(line2);
    }

}

#define BUTTON_COUNT 3
#define MODE_BUTTON_COUNT 20
#define BRAILLE_2ND_LINE_WAIT 25

#define TEST_MODE

void main (void)
{
	WDT_A_hold(WDT_A_BASE); // Stop watchdog timer
	// Minimum Vcore setting required for the USB API is PMM_CORE_LEVEL_2 .
	PMM_setVCore(PMM_BASE, PMM_CORE_LEVEL_2);
	P5SEL |= (BIT3+BIT2);//Crystal Mode
	//	WDT_A_hold(WDT_A_BASE); // Stop watchdog timer
	UCS_XT2Start(UCS_BASE, UCS_XT2DRIVE_16MHZ_24MHZ);
	UCS_setExternalClockSource(UCS_BASE, 32768, 24000000);		//Sets the external clock XT1 and Xt2 frequencies
	UCS_clockSignalInit(UCS_BASE, UCS_ACLK, UCS_VLOCLK_SELECT, UCS_CLOCK_DIVIDER_1);		//Set ACLK source from VLOCLK
	UCS_clockSignalInit(UCS_BASE, UCS_SMCLK, UCS_XT2CLK_SELECT, UCS_CLOCK_DIVIDER_16);		//Set SMCLK source from XT2CLK --> 1Mhz
	UCS_clockSignalInit(UCS_BASE, UCS_MCLK, UCS_XT2CLK_SELECT, UCS_CLOCK_DIVIDER_1);		//Set MCLK source from XT2CLK --> 24Mhz
	UCS_SMCLKOn(UCS_BASE);
	// Minimum Vcore setting required for the USB API is PMM_CORE_LEVEL_2 .
	//	PMM_setVCore(PMM_BASE, PMM_CORE_LEVEL_2);
	initClocks(24000000);   // Config clocks. MCLK=SMCLK=FLL=8MHz; ACLK=REFO=32kHz
	USB_setup(TRUE, TRUE); // Init USB & events; if a host is present, connect
	board_init();
	GPIO_setOutputLowOnPin(DRV_TRG);
	timerdelay(100);
	GPIO_setOutputHighOnPin(DRV_TRG);
	GPIO_setOutputLowOnPin(DRV_TRG);
	DRV_Enable();
	TI_CAPT_Init_Baseline(&buttons);
	TI_CAPT_Update_Baseline(&buttons, 20);
	TI_CAPT_Init_Baseline(&innerWheel);
	TI_CAPT_Update_Baseline(&innerWheel,2);

	update_leds();

#if 0
	while(true)
	{
        int ii = 0;
        for(ii = 0; ii < 8; ii++)
        {
            perform_line_code(ii);
            wait(500);
        }
	}
#endif
#if 0
	while(true)
	{
	    ERM_loop();
	}
#endif
#if 0
    while(true)
    {
        drv_run(Actuator_ERM, DoubleClick_100);
        //perform_line_code(6);
        //DRV_Poll();
        //perform_line_code(7);
        drv_run(Actuator_LRA, DoubleClick_100);
        between_braille_lines();
        //perform_line_code(7);
        //DRV_Poll();
        //perform_line_code(6);
        between_braille_lines();
        int ii = 0;
        for(ii = 0; ii < 8; ii++)
        {
            //perform_line_code(ii);
        }
    }
#endif

#ifdef TEST_MODE
	read_enabled = true;
	while(true)
	{
        buttonPtr = (struct Element *)TI_CAPT_Buttons(&buttons);
        innerPos = TI_CAPT_Wheel(&innerWheel);
        __no_operation();
        if(buttonPtr || innerPos != ILLEGAL_SLIDER_WHEEL_POSITION)
        {
            button_released = false;
            holdcounter++;
            freecounter = 0;
        }
        else
        {

            // Read enabled
            if(read_enabled)
            {
                uint8_t received_byte;
                receive_USB_data(&received_byte);
                if(received_byte)
                {
                    if(received_byte >= '0' && received_byte <= '7')
                    {
                        perform_line_code(received_byte - '0');
                    }
                }
            }
        }

        send_mode_loop();
    }
#else

    while(true)
    {
        buttonPtr = (struct Element *)TI_CAPT_Buttons(&buttons);
        innerPos = TI_CAPT_Wheel(&innerWheel);
        __no_operation();
        if(buttonPtr || innerPos != ILLEGAL_SLIDER_WHEEL_POSITION)
        {
            button_released = false;
            holdcounter++;
            freecounter = 0;
        }
        else
        {
            if(!button_released)
            {
                button_released = true;
                // Released Events

                if(get_down_pressed())
                {
                    if(holdcounter > MODE_BUTTON_COUNT)
                    {
                        change_mode();
                    }
                }
                else
                {
                    // Write enabled
                    if(!read_enabled)
                    {
                        if(holdcounter > BUTTON_COUNT)
                        {
                            if(first_line_fetched)
                            {
                                line2[0] = get_left_pressed();
                                line2[1] = get_select_pressed();
                                line2[2] = get_right_pressed();
                                buttons_fetched = true;
                            }
                            else if(get_minus_pressed() || get_up_pressed() || get_plus_pressed())
                            {
                                line1[0] = get_minus_pressed();
                                line1[1] = get_up_pressed();
                                line1[2] = get_plus_pressed();
                                line2[0] = 0;
                                line2[1] = 0;
                                line2[2] = 0;
                                first_line_fetched = true;
                                buttons_fetched = false;
                            }
                        }

                    }
                }
            }

            // Read enabled
            if(read_enabled)
            {
                uint8_t received_byte;
                receive_USB_data(&received_byte);
                if(received_byte)
                {
                    perform_braille_code(received_byte);
                }
            }
            else
            {
                if(!buttons_fetched && first_line_fetched)
                {
                    freecounter++;
                    if(freecounter > BRAILLE_2ND_LINE_WAIT)
                    {
                        buttons_fetched = true;
                    }
                }

                if(buttons_fetched)
                {
                    uint8_t send_data = braille2char(line1, line2);
                    if(send_data)
                    {
                        send_USB_data(send_data);
                    }
                    buttons_fetched = false;
                    first_line_fetched = false;
                    freecounter = 0;
                }
            }

            reset_buttons();
        }

        send_mode_loop();
    }
#endif
}





/*
 * ======== UNMI_ISR ========
 */
#pragma vector = UNMI_VECTOR
__interrupt VOID UNMI_ISR (VOID)
{
	switch (__even_in_range(SYSUNIV, SYSUNIV_BUSIFG ))
	{
	case SYSUNIV_NONE:
		__no_operation();
		break;
	case SYSUNIV_NMIIFG:
		__no_operation();
		break;
	case SYSUNIV_OFIFG:
		UCS_clearFaultFlag(UCS_BASE, UCS_XT2OFFG);
		UCS_clearFaultFlag(UCS_BASE, UCS_DCOFFG);
		SFR_clearInterrupt(SFR_BASE, SFR_OSCILLATOR_FAULT_INTERRUPT);
		break;
	case SYSUNIV_ACCVIFG:
		__no_operation();
		break;
	case SYSUNIV_BUSIFG:
		// If the CPU accesses USB memory while the USB module is
		// suspended, a "bus error" can occur.  This generates an NMI.  If
		// USB is automatically disconnecting in your software, set a
		// breakpoint here and see if execution hits it.  See the
		// Programmer's Guide for more information.
		SYSBERRIV = 0; //clear bus error flag
		USB_disable(); //Disable
	}
}

//Released_Version_4_00_02
/******************************************************************************/
// Timer1_A0 Interrupt Service Routine:
/******************************************************************************/
#pragma vector=TIMER1_A0_VECTOR
__interrupt void TIMER1_A0_ISR(void)
{
	TIMER_A_stop(PERIODICAL_TIMER);
	TIMER_A_disableCaptureCompareInterrupt(PERIODICAL_TIMER,
			TIMER_A_CAPTURECOMPARE_REGISTER_0);
	//	CapTouch_RandomNumber++;
	__bic_SR_register_on_exit(LPM0_bits+GIE);
}
//******************************************************************************
