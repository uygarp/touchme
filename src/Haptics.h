/*
 * Haptics.h
 *
 *  Created on: Mar 12, 2015
 *      Author: a0221097
 */

#ifndef HAPTICS_H_
#define HAPTICS_H_

#define Actuator_LRA	 0x01
#define Actuator_ERM	 0x00

void DRV_Reset();
void DRV_Run_AutoCAL_ERM();
void DRV_Run_AutoCAL_LRA(void);
void Haptics_Enable(void);
void Haptics_Reset(void);
void DRV_Trigger_High(void);
void DRV_Trigger_Low(void);
void DRV_Trigger_Setup_Intz(void);
void DRV_Run_AutoCAL(void);
void DRV_Run_RTP(uint8_t code, bool m4ermlra);
void Haptics_LoadSwitchSelect(uint8_t actuator);
void DRV_Enable(void);
void DRV_Poll(void);
void Haptics_SendEffect(uint8_t actuator);
void DRV_Send_Actuator_Settings(uint8_t actuatorType);
#endif /* HAPTICS_H_ */
