/*
 * Haptics.c
 *
 *  Created on: Mar 12, 2015
 *      Author: a0221097
 */

#include "gpio.h"
#include "board.h"
#include "LED.h"
#include "string.h"
#include "Board_Functions.h"
#include "Haptics.h"
#include "DRV2625.h"
#include "Actuator_Waveforms.h"

uint16_t hapticErr;
static uint8_t readptr;

//static uint8_t 	actuatorType = Actuator_LRA;		// Actuator Setting


/**
 * Haptics_EnableAmplifier - set enable pin high
 */
void DRV_Enable(void)							// NRST pin = High
{
	GPIO_setOutputHighOnPin(DRV_NRST);
	GPIO_setOutputHighOnPin(LED_NRST);
}

void DRV_Reset(void)							//NRST pin = Low
{
	GPIO_setOutputLowOnPin(DRV_NRST);
	GPIO_setOutputLowOnPin(LED_NRST);
}

void DRV_Trigger_High(void)
{
	GPIO_setAsOutputPin(DRV_TRG);
	GPIO_setOutputHighOnPin(DRV_TRG);
}

void DRV_Trigger_Low(void)
{
	GPIO_setAsOutputPin(DRV_TRG);
	GPIO_setOutputLowOnPin(DRV_TRG);
}

void DRV_Trigger_Setup_Intz(void)
{
	GPIO_setAsInputPinWithPullUpresistor(DRV_TRG);
	GPIO_enableInterrupt(DRV_TRG);
}

void DRV_Send_Actuator_Settings(uint8_t actuatorType)
{

	if(actuatorType == Actuator_ERM)
	{
		StdI2C_P_TX_Single(DRV_ADDR, Rated_Voltage, Voltage_ERM_RV_CL_1p3, &hapticErr);
		StdI2C_P_TX_Single(DRV_ADDR, OD_Clamp, Voltage_ERM_OD_CL_2p7, &hapticErr);
		StdI2C_P_TX_Single(DRV_ADDR, DRV_Interrupt, OC_Detect|Over_TEMP|PRC_Done, &hapticErr);
		StdI2C_P_TX_Single(DRV_ADDR, DRV_Interrupt, OC_Detect|Over_TEMP|PRC_Done, &hapticErr);   //temp
		StdI2C_P_TX_Single(DRV_ADDR, DRV_SET1,I2C_BCAST_Disable|LRA_PERIOD_AVG_DIS|SLEW_RATE_FAST|INTZ_TRG|WVFM_Sequencer  , &hapticErr);
		StdI2C_P_TX_Single(DRV_ADDR, DRV_SET2,ERM_Mode|Closed_Loop|Auto_BRK_OLoop_DIS , &hapticErr);
		StdI2C_P_TX_Single(DRV_ADDR, DRV_SET3, BAT_Life_EXT_Disable|UVLO_Threshold_3p2  , &hapticErr);
		StdI2C_P_TX_Single(DRV_ADDR, ACAL_COMP, 0x0E  , &hapticErr);
		StdI2C_P_TX_Single(DRV_ADDR, ACAL_BEMF, 0x72  , &hapticErr);
		StdI2C_P_TX_Single(DRV_ADDR, DRV_SEL, ERM_Library_OpenLoop  , &hapticErr);
	}
	else if (actuatorType == Actuator_LRA)
	{
		StdI2C_P_TX_Single(DRV_ADDR, Rated_Voltage, VoltageRMS_LRA_RV_2p0, &hapticErr);
		StdI2C_P_TX_Single(DRV_ADDR, OD_Clamp, Voltage_3p3, &hapticErr);
		StdI2C_P_TX_Single(DRV_ADDR, DRV_Interrupt, OC_Detect|Over_TEMP|PRC_Done, &hapticErr);   //temp
		StdI2C_P_TX_Single(DRV_ADDR, DRV_SET1,I2C_BCAST_Disable|LRA_PERIOD_AVG_DIS|SLEW_RATE_FAST|INTZ_TRG|WVFM_Sequencer  , &hapticErr);
		StdI2C_P_TX_Single(DRV_ADDR, DRV_SET2,LRA_Mode|Closed_Loop|Auto_BRK_OLoop_DIS , &hapticErr);
		StdI2C_P_TX_Single(DRV_ADDR, DRV_SET3, BAT_Life_EXT_Disable|UVLO_Threshold_3p2  , &hapticErr);
		StdI2C_P_TX_Single(DRV_ADDR, ACAL_COMP, 0x07  , &hapticErr);
		StdI2C_P_TX_Single(DRV_ADDR, ACAL_BEMF, 0x7C  , &hapticErr);
		StdI2C_P_TX_Single(DRV_ADDR, DRV_SEL, LRA_Library_ClosedLoop  , &hapticErr);}
	else
	{
		// do nothing
	}

}


void DRV_Poll(void)  // can be removed after Process done interrupt function
{
    StdI2C_P_RX_Single(DRV_ADDR, GOBIT, &readptr, &hapticErr);
    while ( readptr== 0x01 )//|| (readptr && 0xF7) != 0x00)
    {
        StdI2C_P_RX_Single(DRV_ADDR, GOBIT, &readptr, &hapticErr);
        timerdelay(100);

    }
}

void DRV_Run_AutoCAL_ERM(void)
{
	Haptics_LoadSwitchSelect(Actuator_ERM);

	StdI2C_P_TX_Single(DRV_ADDR, AutoCal_Time, Time_250ms , &hapticErr);   //temp
	StdI2C_P_TX_Single(DRV_ADDR, Rated_Voltage, 0x5E , &hapticErr);   //temp
	StdI2C_P_TX_Single(DRV_ADDR, OD_Clamp, 0xAA  , &hapticErr);    //temp
	StdI2C_P_TX_Single(DRV_ADDR, DRV_SET2 , (ERM_Mode|Closed_Loop ), &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, DRV_SET1 , INTZ_TRG| ACAL_Routine, &hapticErr);
	StdI2C_P_RX_Single(0x5A, DRV_Status, &readptr, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, GOBIT , GO , &hapticErr);
	readptr = 0x00;
	StdI2C_P_RX_Single(0x5A, DRV_Status, &readptr, &hapticErr);

	while ( (readptr& 0x08)  !=  Process_Done )//|| (readptr && 0xF7) != 0x00)
	{
		StdI2C_P_RX_Single(0x5A, DRV_Status, &readptr, &hapticErr);
		timerdelay(100);

	}

}


void DRV_Run_Sinewave(uint8_t frequency)
{

}


void DRV_Run_AutoCAL_LRA(void)
{
	Haptics_LoadSwitchSelect(Actuator_LRA);
	StdI2C_P_TX_Single(DRV_ADDR, AutoCal_Time, Time_250ms , &hapticErr);   //temp
	StdI2C_P_TX_Single(DRV_ADDR, Rated_Voltage, VoltageRMS_LRA_RV_2p0, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, OD_Clamp, Voltage_3p3, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, DRV_SET2 , (LRA_Mode|Closed_Loop ), &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, DRV_SET1 , INTZ_TRG| ACAL_Routine, &hapticErr);
	StdI2C_P_TX_Single(DRV_ADDR, GOBIT , GO , &hapticErr);
	readptr = 0x00;
	StdI2C_P_RX_Single(0x5A, DRV_Status, &readptr, &hapticErr);

	while ((readptr& 0x08) !=  Process_Done )//|| (readptr && 0xF7) != 0x00)
	{
		StdI2C_P_RX_Single(DRV_ADDR, DRV_Status, &readptr, &hapticErr);
		timerdelay(100);

	}


}
void DRV_Run_RTP(uint8_t code, bool m4ermlra)
{
    DRV_Enable();

	if(m4ermlra)
	{
		DRV_Send_Actuator_Settings(Actuator_ERM);
		Haptics_LoadSwitchSelect(Actuator_ERM);
		StdI2C_P_TX_Single(DRV_ADDR, DRV_SET2 , (ERM_Mode|Closed_Loop ), &hapticErr);
	}
	else
	{
		Haptics_LoadSwitchSelect(Actuator_LRA);
		DRV_Send_Actuator_Settings(Actuator_LRA);
	}

    StdI2C_P_TX_Single(DRV_ADDR, DRV_SET1 , INTZ_TRG| RTP_Mode, &hapticErr);
    StdI2C_P_TX_Single(DRV_ADDR, RTP_Input , code, &hapticErr);
    StdI2C_P_TX_Single(DRV_ADDR, GOBIT , GO , &hapticErr);
}

void Haptics_LoadSwitchSelect(uint8_t actuator)
{
	if(actuator){GPIO_setOutputLowOnPin(DRV_Switch);}					// Select LRA
	else	{GPIO_setOutputHighOnPin(DRV_Switch);}	     		// Select ERM
}


void Haptics_SendEffect(uint8_t actuator)
{
    DRV_Enable();
	if(actuator == Actuator_LRA)
	{
		DRV_Send_Actuator_Settings(Actuator_LRA);
		Haptics_LoadSwitchSelect(Actuator_LRA);
		StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ1, SharpTick1_100  , &hapticErr);
		StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ2, 0x00, &hapticErr);
	}
	else
	{
		DRV_Send_Actuator_Settings(Actuator_ERM);
		Haptics_LoadSwitchSelect(Actuator_ERM);
		StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ1, SharpTick1_100  , &hapticErr);
		StdI2C_P_TX_Single(DRV_ADDR, Waveform_SEQ2, 0x00, &hapticErr);
	}

    StdI2C_P_TX_Single(DRV_ADDR, GOBIT, GO, &hapticErr);
    //DRV_Poll();
}



