#!/bin/python

import time
import threading
import signal
import sys
import usb
import select
import hidraw
import os
from enum import Enum
import random
from PyQt4 import QtCore, QtGui, uic
from PyQt4.QtCore import QTimer

test_ui_file = "touchmeTestUI.ui"
init_ui_file = "touchmeTestInitUI.ui"

vendor_id = "2047"
product_id = "0301"

MAX_DEV_INDEX = 100

g_hidraw_dev = '/dev/hidraw0'
g_usb_connected = False
g_usb_port = "0"
g_read_mode = False;
g_ui = None

number_list = ["0", "1", "2", "3", "4", "5", "6", "7", "0", "1", "2", "3", "4", "5", "6", "7"]
test_index = 0
class State(Enum):
    Learning = 1
    Test = 2
    Finish = 3    
state = State.Learning
results_file_name = "Results.txt"
class Results():
    name = ""
    surname = ""
    first_results = ["-", "-", "-", "-", "-", "-", "-", "-"]
    second_results = ["-", "-", "-", "-", "-", "-", "-", "-"]
    
    def __init__(self, name, surname):
        self.name = name
        self.surname = surname
    
    def update(self, index, value):
        print "self.first_results[index]:" + self.first_results[index] + " ,value:" + str(value)
        if self.first_results[index] is "-":
            self.first_results[index] = value
        elif self.second_results[index] is "-":
            self.second_results[index] = value
    
    def save(self):
        file_results = open(results_file_name, "a+")
        file_results.write("%s %s\r\n" % (self.name, self.surname))
        for i in range(0, 8):
            file_results.write("%s\t" % (self.first_results[i]))
            print str(i) + " : " + self.first_results[i]
        file_results.write("\r\n")
        for i in range(0, 8):
            file_results.write("%s\t" % (self.second_results[i]))
            print str(i) + " : " + self.second_results[i]
        file_results.write("\r\n")
        file_results.write("-------------------------------------------------------------")
        file_results.write("\r\n")
        file_results.close()        

def usb_update_status(vendor_id, product_id):
    global g_usb_connected
    global g_usb_port
    
    _connected = False
    _port = "0"
    for i in range(0, MAX_DEV_INDEX):
        try:
            info = hidraw.get_info(os.open("/dev/hidraw" + str(i), os.O_RDONLY))
        except:
            continue
        if info:
            vendorID = productID = ""
            element = str(info).split(" :")[1]
            if element.startswith("vendor"):
                vendorID = element[-4:]
            element = str(info).split(" :")[2]
            if element.startswith("product"):
                productID = element[-5:-1]
            if(vendorID == vendor_id and productID == product_id):
                _connected = True
                _port = str(i)
                break
    g_usb_connected = _connected
    g_usb_port = _port
    ui_update_status()
    print g_usb_connected, g_usb_port
                

def read_from():
    global g_hidraw_dev
    global g_usb_connected
    global g_read_mode
    global ui_running
    start = end = time.time()
    data_array = ""
    #print "read_from->", " g_usb_port", g_usb_port
    while g_usb_connected and ui_running:
        try:
            data = g_hidraw_dev.read(1)
        except:
            continue
        if data:
            end = time.time()
            if(end - start > 1):
                data_array = ""
                start = time.time()
            
            data_array += data
            if len(data_array) == 3:
                # HERE
                print ">>", data_array[2]
                if data_array[2] is "R":
                    g_read_mode = True
                elif data_array[2] is "W":
                    g_read_mode = False
                elif not g_read_mode:
                    ui_update_text(data_array[2])
                    
def write_output(chr):
    global g_hidraw_dev
    print "<<: " + chr 
    g_hidraw_dev.write("ff" + chr + "ff")

ui_running = False
def main_loop():
    global g_hidraw_dev
    global g_usb_connected
    global ui_running
    connected = False
    
    while ui_running:
        print "ui_running:", ui_running
        usb_update_status(vendor_id, product_id)
        if g_usb_connected:
            #print "1connected:", connected, "g_usb_connected", g_usb_connected
            if not connected:
                connected = True
                #print "Connected", "g_usb_port:", g_usb_port
                g_hidraw_dev = open('/dev/hidraw' + g_usb_port, 'r+b')
                
                reading_thread = threading.Thread(target=read_from, args=())
                #write_thread = threading.Thread(target=write_to, args=())
                if not reading_thread.is_alive():
                    reading_thread.start()
                #if not write_thread.is_alive():
                #    write_thread.start()
                
        else:
            #print "NOT connected"
            print "2connected:", connected, "g_usb_connected", g_usb_connected
            if connected:
                connected = False
                reading_thread.join()
                #write_thread.join()
                g_hidraw_dev.close()
            
        time.sleep(3)

def jobs_after_question():
    global TestWindow
    enable_Buttons(True)
    TestWindow.label.setText("Select a button for signal you feel.")

def goTest():
    global TestWindow, test_index, number_list, test_thread, results
    enable_Buttons(False)
    if test_index is len(number_list):
    #if test_index is 1:
        results.save()
        TestWindow.label.setText("You have finished the test. Press \"x\" to exit...")
        return
    TestWindow.groupBox.setTitle("Test Part " + "(" + str(test_index + 1) + " / " + str(len(number_list)) + ")")
    TestWindow.label.setText("Try to feel...")
    #QTimer.singleShot(1500, lambda: jobs_after_question())
    chr = number_list[test_index]
    print "test_index:" + str(test_index) + "chr=" + chr
    QTimer.singleShot(50, lambda: write_output(chr))
    QTimer.singleShot(1500, lambda: jobs_after_question())
    #test_index = test_index + 1
    
def button_clicked(chr):
    global state, test_index, number_list, results
    print str(state)
    if state is State.Test:
        result_index = int(number_list[test_index])
        print "test_index:" + str(test_index) + "result_index:" + str(result_index)
        results.update(result_index, chr)
        test_index = test_index + 1
        goTest()
    if state is State.Learning:
        enable_Buttons(False)
        QTimer.singleShot(500, lambda: enable_Buttons(True))
        QTimer.singleShot(5, lambda: write_output(chr))        
    
def next_clicked():
    global state
    global TestWindow
    if state is State.Learning:
        state = State.Test
        enable_Buttons(True)
        TestWindow.groupBox.setTitle("Test Part " + "(" + str(test_index + 1) + " / " + str(len(number_list)) + ")")
        TestWindow.nextButton.setVisible(False)

        random.shuffle(number_list)
        print "Random: " + str(number_list)
        goTest()
    if state is State.Finish:
        print "Saving..."

def ui_update_text(chr):
    global TestWindow
    try:
        print "ui_update_text:", chr
        TestWindow.textRead.insertPlainText(chr)
    except:
        return
        
def ui_update_status():
    global TestWindow
    global state
    try:
        if g_usb_connected:
            status_text = "Connected, "
            status_text += "Read" if g_read_mode else "Write"
            status_text += " Mode"
        else:
            status_text = "Disconnected"
        print status_text
        TestWindow.statusbar.showMessage(status_text)
        TestWindow.groupBox.setEnabled(g_usb_connected and g_read_mode)
    except:
        return
        
def enable_Buttons(enable):
    global TestWindow
    TestWindow.pushButton_1.setEnabled(enable)
    TestWindow.pushButton_2.setEnabled(enable)
    TestWindow.pushButton_3.setEnabled(enable)
    TestWindow.pushButton_4.setEnabled(enable)
    TestWindow.pushButton_5.setEnabled(enable)
    TestWindow.pushButton_6.setEnabled(enable)
    TestWindow.pushButton_7.setEnabled(enable)
    TestWindow.pushButton_8.setEnabled(enable)
    
def app_ui_delegation():
    global TestWindow
    TestWindow.pushButton_1.clicked.connect(lambda: button_clicked("0"))
    TestWindow.pushButton_2.clicked.connect(lambda: button_clicked("1"))
    TestWindow.pushButton_3.clicked.connect(lambda: button_clicked("2"))
    TestWindow.pushButton_4.clicked.connect(lambda: button_clicked("3"))
    TestWindow.pushButton_5.clicked.connect(lambda: button_clicked("4"))
    TestWindow.pushButton_6.clicked.connect(lambda: button_clicked("5"))
    TestWindow.pushButton_7.clicked.connect(lambda: button_clicked("6"))
    TestWindow.pushButton_8.clicked.connect(lambda: button_clicked("7"))
    TestWindow.nextButton.clicked.connect(lambda: next_clicked())
    
def is_name_valid(name, surname):
    ret = False
    if name != "" and surname != "":
        global g_name, g_surname
        g_name = name
        g_surname = surname
        ret = True
    return ret

def init_completed(start_pressed, name, surname):
    global test_init_ret
    test_init_ret = start_pressed and is_name_valid(name, surname)
    if start_pressed and not is_name_valid(name, surname):
        print("Name error!...")
    else:
        close_init()
    
def create_init(app):
    global TestInitWindow, test_init_ret
    test_init_ret = False
    TestInitWindow = uic.loadUi(init_ui_file)
    TestInitWindow.button_start.clicked.connect(lambda: init_completed(True, TestInitWindow.edit_name.text(), TestInitWindow.edit_surname.text()))
    TestInitWindow.button_cancel.clicked.connect(lambda: init_completed(False, "", ""))
    TestInitWindow.show()
    app.exec_()
    return 0 if test_init_ret else -1

def close_init():
    global TestInitWindow
    TestInitWindow.close()

def create_test(app):
    global TestWindow, g_name, g_surname, results
    results = Results(g_name, g_surname)
    main_thread = threading.Thread(target=main_loop, args=())
    if not main_thread.is_alive():
        main_thread.start()
        
    TestWindow = uic.loadUi(test_ui_file)
    
    ### CENTERING
    frameGm = TestWindow.frameGeometry()
    screen = app.desktop().screenNumber(app.desktop().cursor().pos())
    centerPoint = app.desktop().screenGeometry(screen).center()
    frameGm.moveCenter(centerPoint)
    TestWindow.move(frameGm.topLeft())
    
    app_ui_delegation()
    TestWindow.show()
    return app.exec_()

if __name__ == '__main__':
    ui_running = True

    QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_X11InitThreads)
    app = QtGui.QApplication(sys.argv)

    g_name = g_surname = ""
    ret = create_init(app)
    if ret == 0:
        ret = create_test(app)
    ui_running = False
    
    try:
        sys.exit()
    except SystemExit:
        print("Exit...")
    except:
        print("An unknown error occurred...")
    
    
    
