#!/bin/python

import time
import threading
import signal
import sys
import usb
import select
import hidraw
import os

from PyQt4 import QtCore, QtGui
from touchmeUI import Ui_MainWindow

vendor_id = "2047"
product_id = "0301"

MAX_DEV_INDEX = 100

g_hidraw_dev = '/dev/hidraw0'
g_usb_connected = False
g_usb_port = "0"
g_read_mode = False;
g_ui = None

def usb_update_status(vendor_id, product_id):
    global g_usb_connected
    global g_usb_port
    
    _connected = False
    _port = "0"
    for i in range(0, MAX_DEV_INDEX):
        try:
            info = hidraw.get_info(os.open("/dev/hidraw" + str(i), os.O_RDONLY))
        except:
            continue
        if info:
            vendorID = productID = ""
            element = str(info).split(" :")[1]
            if element.startswith("vendor"):
                vendorID = element[-4:]
            element = str(info).split(" :")[2]
            if element.startswith("product"):
                productID = element[-5:-1]
            if(vendorID == vendor_id and productID == product_id):
                _connected = True
                _port = str(i)
                break
    g_usb_connected = _connected
    g_usb_port = _port
    ui_update_status()
    print g_usb_connected, g_usb_port
                

def read_from():
    global g_hidraw_dev
    global g_usb_connected
    global g_read_mode
    global ui_running
    start = end = time.time()
    data_array = ""
    #print "read_from->", " g_usb_port", g_usb_port
    while g_usb_connected and ui_running:
        try:
            data = g_hidraw_dev.read(1)
        except:
            continue
        if data:
            end = time.time()
            if(end - start > 1):
                data_array = ""
                start = time.time()
            
            data_array += data
            if len(data_array) == 3:
                # HERE
                print ">>", data_array[2]
                if data_array[2] is "R":
                    g_read_mode = True
                elif data_array[2] is "W":
                    g_read_mode = False
                elif not g_read_mode:
                    ui_update_text(data_array[2])
                    
def write_output(chr):
    global g_hidraw_dev
    g_hidraw_dev.write("ff" + chr + "ff")

#def write_to():
#    global g_hidraw_dev
#    return

ui_running = False

def main_loop():
    global g_hidraw_dev
    global g_usb_connected
    global ui_running
    connected = False
    
    while ui_running:
        print "ui_running:", ui_running
        usb_update_status(vendor_id, product_id)
        if g_usb_connected:
            #print "1connected:", connected, "g_usb_connected", g_usb_connected
            if not connected:
                connected = True
                #print "Connected", "g_usb_port:", g_usb_port
                g_hidraw_dev = open('/dev/hidraw' + g_usb_port, 'r+b')
                
                reading_thread = threading.Thread(target=read_from, args=())
                #write_thread = threading.Thread(target=write_to, args=())
                if not reading_thread.is_alive():
                    reading_thread.start()
                #if not write_thread.is_alive():
                #    write_thread.start()
                
        else:
            #print "NOT connected"
            print "2connected:", connected, "g_usb_connected", g_usb_connected
            if connected:
                connected = False
                reading_thread.join()
                #write_thread.join()
                g_hidraw_dev.close()
            
        time.sleep(3)

    print "1ui_running:", ui_running
    print "bitti"

def button_clicked(chr):
    print chr, "pressed"
    write_output(chr)


def ui_update_text(chr):
    global ui
    try:
        print "ui_update_text:", chr
        ui.textRead.insertPlainText(chr)
    except:
        return
        
def ui_update_status():
    global ui
    try:
        if g_usb_connected:
            status_text = "Connected, "
            status_text += "Read" if g_read_mode else "Write"
            status_text += " Mode"
        else:
            status_text = "Disconnected"
        ui.statusbar.showMessage(status_text)
        ui.groupBox_read.setEnabled(g_usb_connected and g_read_mode)
        ui.groupBox_write.setEnabled(g_usb_connected and not g_read_mode)
    except:
        return

def app_ui_delegation(ui):
    ui.pushButton_a.clicked.connect(lambda: button_clicked("a"))
    ui.pushButton_b.clicked.connect(lambda: button_clicked("b"))
    ui.pushButton_c.clicked.connect(lambda: button_clicked("c"))
    ui.pushButton_d.clicked.connect(lambda: button_clicked("d"))
    ui.pushButton_e.clicked.connect(lambda: button_clicked("e"))
    ui.pushButton_f.clicked.connect(lambda: button_clicked("f"))
    ui.pushButton_g.clicked.connect(lambda: button_clicked("g"))
    ui.pushButton_h.clicked.connect(lambda: button_clicked("h"))
    ui.pushButton_i.clicked.connect(lambda: button_clicked("i"))
    ui.pushButton_j.clicked.connect(lambda: button_clicked("j"))
    ui.pushButton_k.clicked.connect(lambda: button_clicked("k"))
    ui.pushButton_l.clicked.connect(lambda: button_clicked("l"))
    ui.pushButton_m.clicked.connect(lambda: button_clicked("m"))
    ui.pushButton_n.clicked.connect(lambda: button_clicked("n"))
    ui.pushButton_o.clicked.connect(lambda: button_clicked("o"))
    ui.pushButton_p.clicked.connect(lambda: button_clicked("p"))
    ui.pushButton_q.clicked.connect(lambda: button_clicked("q"))
    ui.pushButton_r.clicked.connect(lambda: button_clicked("r"))
    ui.pushButton_s.clicked.connect(lambda: button_clicked("s"))
    ui.pushButton_t.clicked.connect(lambda: button_clicked("t"))
    ui.pushButton_u.clicked.connect(lambda: button_clicked("u"))
    ui.pushButton_v.clicked.connect(lambda: button_clicked("v"))
    ui.pushButton_w.clicked.connect(lambda: button_clicked("w"))
    ui.pushButton_x.clicked.connect(lambda: button_clicked("x"))
    ui.pushButton_y.clicked.connect(lambda: button_clicked("y"))
    ui.pushButton_z.clicked.connect(lambda: button_clicked("z"))
    
if __name__ == '__main__':
    #main_loop()
    ui_running = True
    
    main_thread = threading.Thread(target=main_loop, args=())
    if not main_thread.is_alive():
        main_thread.start()
    
    QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_X11InitThreads)
    app = QtGui.QApplication(sys.argv)
    MainWindow = QtGui.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    app_ui_delegation(ui)
    MainWindow.show()
    
    sys.exit(app.exec_())
    ui_running = False
    
    
    
